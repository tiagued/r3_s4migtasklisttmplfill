﻿Imports System.Reflection
Imports Microsoft.Office.Interop.Excel
Imports System.Data.OleDb

Public Class CReadTransformController
    Public glb_oconn As System.Data.OleDb.OleDbConnection
    Public ctrl As CMasterController
    Public bow_entry_dict As Dictionary(Of String, BOWEntry)
    Public migrated_material_list As List(Of String)
    'list of source items with error
    Public list_src_error As List(Of String)
    'list of mbx docs
    Public list_mbx_doc As Dictionary(Of String, CTmplEntry)
    Dim countTmplEntries As Long
    Public Sub New(_ctr As CMasterController)
        ctrl = _ctr
        list_src_error = New List(Of String)
        list_mbx_doc = New Dictionary(Of String, CTmplEntry)
        bow_entry_dict = New Dictionary(Of String, BOWEntry)
        migrated_material_list = New List(Of String)
    End Sub

    Public Sub loadAndTransformSourceData()
        'init worbook and worksheet to read GQT
        Dim wb As Microsoft.Office.Interop.Excel.Workbook = Nothing
        Dim wb_sht As Microsoft.Office.Interop.Excel.Worksheet = Nothing
        Dim Master_MaxNumberOfLogs = 10
        Dim Master_entriesInOneLog As Double = ctrl.confController.masterList.Count / Master_MaxNumberOfLogs
        Dim Master_logCount As Long = 0
        Dim dt As System.Data.DataTable = Nothing
        Dim Master_line = 1
        Dim Master_Increment = 8 '80%

        If ctrl.check_mat AndAlso migrated_material_list.Count = 0 Then
            ctrl.addDetailsLog("Load S4 Material List")
            MMaterialList.loadMaterialList(Me)
        End If

        If ctrl.confController.masterList.Count < 10 Then
            Master_Increment = 8 * 10 / ctrl.confController.masterList.Count
        End If

        Dim etl_to_panel As Dictionary(Of String, String) = Nothing
        'use to check if one code already exists with another htl
        Dim code_to_codeHtl_list As Dictionary(Of String, List(Of String)) = Nothing
        If Master_entriesInOneLog <= 1 Then
            Master_entriesInOneLog = 1
        End If
        For Each masterConfObj As CMasterConfObj In ctrl.confController.masterList
            Try
                'clear statistics
                ctrl.source_cnt = 0
                ctrl.success_cnt = 0
                ctrl.failed_cnt = 0
                ctrl.ignored_cnt = 0
                ctrl.duplicated_serv_cnt = 0
                ctrl.logs_cnt = 0
                ctrl.mbx_doc_cnt = 0
                ctrl.htl_cnt = 0
                ctrl.etl_cnt = 0
                ctrl.htl_rel_cnt = 0
                ctrl.related_not_found_cnt = 0
                ctrl.operation_cnt = 0
                ctrl.material_cnt = 0
                ctrl.material_ignored_cnt = 0
                ctrl.related_serv_ignored_cnt = 0
                ctrl.serv_task_ignored_cnt = 0
                ctrl.infinite_link_cnt = 0
                ctrl.child_related_of_child_cnt = 0
                ctrl.infinite_circular_link_cnt = 0
                ctrl.logGlobalList.Clear()
                'sub progress bar label
                ctrl.mainForm.sub_progress_bar_lbl.Text = masterConfObj.bow_file_path
                ctrl.setSubProgressBar(0)
                'clear data
                list_mbx_doc.Clear()
                list_src_error.Clear()
                bow_entry_dict.Clear()
                Dim panel_list As New Dictionary(Of String, BOWEntry)
                bow_entry_dict.Clear()
                Dim mainApplObj = masterConfObj.appl_list(0)
                Dim line_start As Integer = 0
                Dim line_end As Integer = 0
                Dim continue_loop As Boolean = False
                countTmplEntries = 0
                'read data from R3 BOW file
                'check if wb exists
                If Not IO.File.Exists(masterConfObj.bow_file_path) Then
                    Throw New Exception("The source file " & masterConfObj.bow_file_path & " does Not exists")
                End If
                ctrl.addDetailsLog("Open R3 BOW file " & masterConfObj.bow_file_path)
                dt = New System.Data.DataTable
                Using conn = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & masterConfObj.bow_file_path & ";Extended Properties=""Excel 12.0 Xml;HDR=YES""")
                    conn.Open()
                    Dim query = "Select * FROM [" & masterConfObj.bow_tab & "$];"
                    Using cmd As New System.Data.OleDb.OleDbCommand(query, conn)
                        dt.Load(cmd.ExecuteReader)
                    End Using
                End Using
                'log data loading
                ctrl.setSubProgressBar(15)

                continue_loop = dt.Rows.Count > 0
                'log
                ctrl.addDetailsLog("Start reading BOW data. Total : " & dt.Rows.Count)

                Dim MaxNumberOfLogs = 10
                Dim entriesInOneLog As Double = dt.Rows.Count / MaxNumberOfLogs
                Dim logCount As Long = 0
                Dim line = 0
                etl_to_panel = New Dictionary(Of String, String)
                code_to_codeHtl_list = New Dictionary(Of String, List(Of String))
                While continue_loop
                    Dim bow_entry = New BOWEntry
                    bow_entry.line = line + 2
                    'count source
                    ctrl.source_cnt = ctrl.source_cnt + 1
                    For Each key As String In ctrl.confController.srcDataConfList.Keys
                        Dim value As Object = dt.Rows(line)(CInt(ctrl.confController.srcDataConfList(key)))
                        If Not IsDBNull(value) Then
                            CallByName(bow_entry, key, CallType.Let, New Object() {value})
                        End If
                    Next
                    'clean description text
                    If Not String.IsNullOrWhiteSpace(bow_entry.description) Then
                        bow_entry.description = bow_entry.description.Replace("""", "")

                        bow_entry.description = bow_entry.description.Replace(" , ", "-")
                        bow_entry.description = bow_entry.description.Replace(", ", "-")
                        bow_entry.description = bow_entry.description.Replace(" ,", "-")

                        bow_entry.description = bow_entry.description.Replace(" / ", "/")
                        bow_entry.description = bow_entry.description.Replace("/ ", "/")
                        bow_entry.description = bow_entry.description.Replace(" /", "/")

                        bow_entry.description = bow_entry.description.Replace(" - ", "-")
                        bow_entry.description = bow_entry.description.Replace("- ", "-")
                        bow_entry.description = bow_entry.description.Replace(" -", "-")
                    End If

                    If Not String.IsNullOrWhiteSpace(bow_entry.etl_id) AndAlso etl_to_panel.ContainsKey(bow_entry.etl_id) Then
                        bow_entry.panel = etl_to_panel(bow_entry.etl_id)
                    End If
                    'add to htl list if does not exist
                    If Not bow_entry_dict.ContainsKey(bow_entry.code_htl) Then
                        'check that it is an htl not an etl
                        If String.IsNullOrWhiteSpace(bow_entry.notif_type) Then
                            Dim Log As New CLogEntry
                            Log.line = bow_entry.line
                            Log.mat_id = bow_entry.htl_id
                            Log.code = bow_entry.code
                            Log.column = "notif type"
                            Log.log_type = CLogEntry.LOG_TYPE_ERROR
                            Log.description = "The header of the task does not have a notification type"
                            ctrl.logGlobalList.Add(Log)
                            ctrl.failed_cnt = ctrl.failed_cnt + 1
                        End If
                        If code_to_codeHtl_list.ContainsKey(bow_entry.code) Then
                            If code_to_codeHtl_list(bow_entry.code).Count = 1 Then
                                bow_entry_dict.Add(bow_entry.code_htl, bow_entry)
                                code_to_codeHtl_list(bow_entry.code).Add(bow_entry.code_htl)
                                'set code with suffix
                                bow_entry.virtual_code = bow_entry.code & "_2"
                                bow_entry_dict(code_to_codeHtl_list(bow_entry.code)(0)).virtual_code = bow_entry_dict(code_to_codeHtl_list(bow_entry.code)(0)).virtual_code & "_1"
                            Else
                                bow_entry_dict.Add(bow_entry.code_htl, bow_entry)
                                code_to_codeHtl_list(bow_entry.code).Add(bow_entry.code_htl)
                                'set code with suffix
                                bow_entry.virtual_code = bow_entry.code & "_" & code_to_codeHtl_list(bow_entry.code).Count
                            End If
                        Else
                            bow_entry_dict.Add(bow_entry.code_htl, bow_entry)
                            bow_entry.virtual_code = bow_entry.code
                            code_to_codeHtl_list.Add(bow_entry.code, New String() {bow_entry.code_htl}.ToList)
                        End If
                    Else
                        'if htl already exists
                        'check if this is an ETL
                        If Not String.IsNullOrWhiteSpace(bow_entry.etl_id) AndAlso ((String.IsNullOrWhiteSpace(bow_entry.panel) AndAlso Not bow_entry_dict(bow_entry.code_htl).etl_list.ContainsKey(bow_entry.etl_id)) OrElse
                            (Not String.IsNullOrWhiteSpace(bow_entry.panel) AndAlso (Not panel_list.ContainsKey(bow_entry.panel) OrElse Not panel_list(bow_entry.panel).etl_list.ContainsKey(bow_entry.etl_id) OrElse Not bow_entry_dict(bow_entry.code_htl).panel_list.ContainsKey(bow_entry.panel)))) Then
                            'if there is an ETL number, then process the Panel/ETL/Op/mat
                            'handle panels
                            If Not String.IsNullOrWhiteSpace(bow_entry.panel) Then
                                'add in panel header list if not exist
                                If Not panel_list.ContainsKey(bow_entry.panel) Then
                                    panel_list.Add(bow_entry.panel, bow_entry)
                                    bow_entry.notif_type = "RO"
                                    bow_entry.ata_chapter = "20"
                                    bow_entry.src_ref = bow_entry_dict(bow_entry.code_htl).src_ref
                                    bow_entry.src_rev = bow_entry_dict(bow_entry.code_htl).src_rev
                                    bow_entry.src_type = bow_entry_dict(bow_entry.code_htl).src_type
                                End If
                                If Not etl_to_panel.ContainsKey(bow_entry.etl_id) Then
                                    etl_to_panel.Add(bow_entry.etl_id, bow_entry.panel)
                                End If
                                'add panel to htl if not exists
                                If Not bow_entry_dict(bow_entry.code_htl).panel_list.ContainsKey(bow_entry.panel) Then
                                    bow_entry_dict(bow_entry.code_htl).panel_list.Add(bow_entry.panel, bow_entry)
                                End If
                                'handle panel's ETLs
                                If Not String.IsNullOrWhiteSpace(bow_entry.etl_id) AndAlso Not panel_list(bow_entry.panel).etl_list.ContainsKey(bow_entry.etl_id) Then
                                    panel_list(bow_entry.panel).etl_list.Add(bow_entry.etl_id, bow_entry)
                                End If
                            Else
                                'if not a panel, add it to etl list
                                bow_entry_dict(bow_entry.code_htl).etl_list.Add(bow_entry.etl_id, bow_entry)
                            End If
                            If String.IsNullOrWhiteSpace(bow_entry.pm_ps) Then
                                Dim Log As New CLogEntry
                                Log.line = bow_entry.line
                                Log.mat_id = bow_entry.htl_id
                                Log.code = bow_entry.code
                                Log.column = "etl_id"
                                Log.log_type = CLogEntry.LOG_TYPE_ERROR
                                Log.description = "The ETL task / PM PS Ref. is missing on the header " & bow_entry.htl_id
                                ctrl.logGlobalList.Add(Log)
                                ctrl.failed_cnt = ctrl.failed_cnt + 1
                            End If
                        Else
                            'if etl header / panel exists, process operations and material
                            'if operation
                            If Not String.IsNullOrWhiteSpace(bow_entry.etl_id) AndAlso Not String.IsNullOrWhiteSpace(bow_entry.work_center_txt) Then
                                'if panel
                                If Not String.IsNullOrWhiteSpace(bow_entry.panel) AndAlso panel_list.ContainsKey(bow_entry.panel) Then
                                    'only one operation
                                    If panel_list(bow_entry.panel).etl_list(bow_entry.etl_id).operation_list.Count = 0 Then
                                        panel_list(bow_entry.panel).etl_list(bow_entry.etl_id).operation_list.Add(bow_entry.operation, bow_entry)
                                    Else
                                        'Dim Log As New CLogEntry
                                        'Log.line = bow_entry.line
                                        'Log.mat_id = bow_entry.htl_id
                                        'Log.code = bow_entry.code
                                        'Log.column = "line"
                                        'Log.log_type = CLogEntry.LOG_TYPE_WARNING
                                        'Log.description = "Line " & bow_entry.line & " An operation already exists on the Panel " & bow_entry.panel
                                        'ctrl.logGlobalList.Add(Log)
                                        ctrl.ignored_cnt = ctrl.ignored_cnt + 1
                                    End If
                                Else
                                    'if normal task
                                    If Not bow_entry_dict(bow_entry.code_htl).etl_list(bow_entry.etl_id).operation_list.ContainsKey(bow_entry.operation) Then
                                        bow_entry_dict(bow_entry.code_htl).etl_list(bow_entry.etl_id).operation_list.Add(bow_entry.operation, bow_entry)
                                    Else
                                        Dim Log As New CLogEntry
                                        Log.line = bow_entry.line
                                        Log.mat_id = bow_entry.htl_id
                                        Log.code = bow_entry.code
                                        Log.column = "line"
                                        Log.log_type = CLogEntry.LOG_TYPE_WARNING
                                        Log.description = "Line " & bow_entry.line & " has been ignored - Operation " & bow_entry.operation & " appears more than once"
                                        ctrl.logGlobalList.Add(Log)
                                        ctrl.ignored_cnt = ctrl.ignored_cnt + 1
                                    End If
                                End If
                            ElseIf Not String.IsNullOrWhiteSpace(bow_entry.etl_id) AndAlso Not String.IsNullOrWhiteSpace(bow_entry.part_number) Then
                                'material
                                'if panel
                                If Not String.IsNullOrWhiteSpace(bow_entry.panel) AndAlso panel_list.ContainsKey(bow_entry.panel) Then
                                    If Not panel_list(bow_entry.panel).etl_list(bow_entry.etl_id).operation_list.ContainsKey(bow_entry.operation) Then
                                        Dim Log As New CLogEntry
                                        Log.line = bow_entry.line
                                        Log.mat_id = bow_entry.htl_id
                                        Log.code = bow_entry.code
                                        Log.column = "operation"
                                        Log.log_type = CLogEntry.LOG_TYPE_ERROR
                                        Log.description = "Operation " & bow_entry.operation & " is missing on ETL " & bow_entry.etl_id
                                        ctrl.logGlobalList.Add(Log)
                                        ctrl.failed_cnt = ctrl.failed_cnt + 1
                                    Else
                                        'if PN not appears
                                        If Not panel_list(bow_entry.panel).etl_list(bow_entry.etl_id).operation_list(bow_entry.operation).material_list.ContainsKey(bow_entry.part_number) Then
                                            panel_list(bow_entry.panel).etl_list(bow_entry.etl_id).operation_list(bow_entry.operation).material_list.Add(bow_entry.part_number, bow_entry)
                                        Else
                                            Dim Log As New CLogEntry
                                            Log.line = bow_entry.line
                                            Log.mat_id = bow_entry.htl_id
                                            Log.code = bow_entry.code
                                            Log.column = "line"
                                            Log.log_type = CLogEntry.LOG_TYPE_WARNING
                                            Log.description = "Line " & bow_entry.line & " has been ignored - PN " & bow_entry.part_number & " appears more than once on the Panel " & bow_entry.panel
                                            ctrl.logGlobalList.Add(Log)
                                            ctrl.ignored_cnt = ctrl.ignored_cnt + 1
                                        End If
                                    End If
                                Else
                                    If Not bow_entry_dict(bow_entry.code_htl).etl_list(bow_entry.etl_id).operation_list.ContainsKey(bow_entry.operation) Then
                                        Dim Log As New CLogEntry
                                        Log.line = bow_entry.line
                                        Log.mat_id = bow_entry.htl_id
                                        Log.code = bow_entry.code
                                        Log.column = "operation"
                                        Log.log_type = CLogEntry.LOG_TYPE_ERROR
                                        Log.description = "Operation " & bow_entry.operation & " is missing on ETL " & bow_entry.etl_id
                                        ctrl.logGlobalList.Add(Log)
                                        ctrl.failed_cnt = ctrl.failed_cnt + 1
                                    Else
                                        'if PN not appears
                                        If Not bow_entry_dict(bow_entry.code_htl).etl_list(bow_entry.etl_id).operation_list(bow_entry.operation).material_list.ContainsKey(bow_entry.part_number) Then
                                            bow_entry_dict(bow_entry.code_htl).etl_list(bow_entry.etl_id).operation_list(bow_entry.operation).material_list.Add(bow_entry.part_number, bow_entry)
                                        Else
                                            Dim Log As New CLogEntry
                                            Log.line = bow_entry.line
                                            Log.mat_id = bow_entry.htl_id
                                            Log.code = bow_entry.code
                                            Log.column = "line"
                                            Log.log_type = CLogEntry.LOG_TYPE_WARNING
                                            Log.description = "Line " & bow_entry.line & " has been ignored - PN " & bow_entry.part_number & " appears more than once"
                                            ctrl.logGlobalList.Add(Log)
                                            ctrl.ignored_cnt = ctrl.ignored_cnt + 1
                                        End If
                                    End If
                                End If
                            Else
                                'when a panel appears more than once, avoid to flag it as an error
                                If Not etl_to_panel.ContainsKey(bow_entry.etl_id) Then
                                    Dim Log As New CLogEntry
                                    If bow_entry_dict.ContainsKey(bow_entry.code_htl) AndAlso String.IsNullOrWhiteSpace(bow_entry.etl_id) Then
                                        Log.line = bow_entry.line
                                        Log.mat_id = bow_entry.htl_id
                                        Log.code = bow_entry.code
                                        Log.column = "line"
                                        Log.log_type = CLogEntry.LOG_TYPE_WARNING
                                        Log.description = "Line " & bow_entry.line & " has been ignored - The code " & bow_entry.code & " appears more than once"
                                        ctrl.logGlobalList.Add(Log)
                                        ctrl.ignored_cnt = ctrl.ignored_cnt + 1
                                    Else
                                        Log.line = bow_entry.line
                                        Log.mat_id = bow_entry.htl_id
                                        Log.code = bow_entry.code
                                        Log.column = "line"
                                        Log.log_type = CLogEntry.LOG_TYPE_WARNING
                                        Log.description = "Line " & bow_entry.line & " was not recognized"
                                        ctrl.logGlobalList.Add(Log)
                                        ctrl.failed_cnt = ctrl.failed_cnt + 1
                                    End If
                                Else
                                    'Dim Log As New CLogEntry
                                    'Log.line = bow_entry.line
                                    'Log.mat_id = bow_entry.htl_id
                                    'Log.code = bow_entry.code
                                    'Log.column = "line"
                                    'Log.log_type = CLogEntry.LOG_TYPE_WARNING
                                    'Log.description = "Line " & bow_entry.line & " has been ignored - Panel " & bow_entry.panel & " already exists"
                                    'ctrl.logGlobalList.Add(Log)
                                    ctrl.ignored_cnt = ctrl.ignored_cnt + 1
                                End If
                            End If
                        End If
                    End If

                    'log
                    If (line) >= entriesInOneLog * (logCount + 1) Then
                        ctrl.addDetailsLog("GQT lines have read from database " & line & ", " & line_end - line & " are remaning")
                        logCount = logCount + 1
                        'increment progress 15% for read DB, 20 for organizing data, 30 for transforming, 30 for transposing, 5 for saving
                        ctrl.incrementSubProgressBar(2) '2*10=20
                    End If

                    'continue flag ?
                    continue_loop = line < dt.Rows.Count - 1
                    line = line + 1
                End While

                'log
                ctrl.setSubProgressBar(35)

                ctrl.addDetailsLog("Start transforming BOW data")

                MaxNumberOfLogs = 10
                entriesInOneLog = (panel_list.Count + bow_entry_dict.Count) / MaxNumberOfLogs
                logCount = 0
                line = 1

                Dim mbx_entry As CTmplEntry
                Dim htl_entry As CTmplEntry
                Dim etl_entry As CTmplEntry
                For Each bow_entry In panel_list.Values
                    'flag to tell if the line from GQT has been processed with success
                    Dim successfully_transformed As Boolean = True
                    Dim success_transform_op As Boolean = True
                    Dim success_transform_mat As Boolean = True
                    Dim this_count As Integer = 0
                    Dim ruleResultStr As KeyValuePair(Of String, CLogEntry) = Nothing
                    'fill mbx
                    'check if the item has not yet been processed
                    If Not list_src_error.Contains(bow_entry.panel) Then
                        mbx_entry = New CTmplEntry
                        htl_entry = New CTmplEntry
                        etl_entry = New CTmplEntry

                        mbx_entry.htl_entry = htl_entry
                        mbx_entry.etl_entry = etl_entry

                        successfully_transformed = fillMBX(masterConfObj, mainApplObj, mbx_entry, bow_entry)
                        successfully_transformed = successfully_transformed AndAlso fillHTL(masterConfObj, mainApplObj, htl_entry, bow_entry)
                        successfully_transformed = successfully_transformed AndAlso fillETL(masterConfObj, mainApplObj, etl_entry, bow_entry)

                        htl_entry.key_date = mbx_entry.issue_dt
                        htl_entry.tl_description = UCase("REM/INST Panel " & bow_entry.panel)

                        'create close panel
                        etl_entry.is_panel = True
                        etl_entry.gqt_entry = bow_entry
                        etl_entry.etl_entry_close_panel = New CTmplEntry
                        successfully_transformed = successfully_transformed AndAlso fillETL(masterConfObj, mainApplObj, etl_entry.etl_entry_close_panel, bow_entry)

                        mbx_entry.source_id = ctrl.confController.defaultValueList("mbx_doc_src_id_pref") & "_" & masterConfObj.src_id_ac_type_text & "_" & bow_entry.panel
                        htl_entry.source_id = htl_entry.m_type & "_" & masterConfObj.src_id_ac_type_text & "_" & bow_entry.panel
                        etl_entry.source_id = etl_entry.m_type & "_" & masterConfObj.src_id_ac_type_text & "_" & bow_entry.panel & "_R"
                        'etl close panel
                        etl_entry.etl_entry_close_panel.source_id = etl_entry.m_type & "_" & masterConfObj.src_id_ac_type_text & "_" & bow_entry.panel & "_I"
                        If Len(etl_entry.etl_entry_close_panel.source_id) > 40 Then
                            successfully_transformed = False
                            Dim Log As New CLogEntry
                            Log.line = bow_entry.line
                            Log.mat_id = bow_entry.panel
                            Log.code = bow_entry.panel
                            Log.column = "External Reference"
                            Log.log_type = CLogEntry.LOG_TYPE_ERROR
                            Log.description = "Source_id has more than 40 characters: " & etl_entry.etl_entry_close_panel.source_id
                            ctrl.logGlobalList.Add(Log)
                        End If
                        'link with htl mbx
                        htl_entry.dir_source_id = mbx_entry.source_id
                        mbx_entry.child_tl_ref_id = htl_entry.source_id

                        'mbx des, lng desc
                        mbx_entry.description = "REM/INST PANEL " & bow_entry.panel
                        MMigrationRules.setMBXDocDescriptionRules(mbx_entry)
                        'mbx external ref
                        mbx_entry.external_ref_no = bow_entry.panel
                        mbx_entry.oem_ja_code = bow_entry.panel
                        htl_entry.tl_ext_id = bow_entry.panel
                        etl_entry.tl_ext_id = bow_entry.panel
                        'htl etl issue date
                        etl_entry.key_date = mbx_entry.issue_dt
                        etl_entry.etl_entry_close_panel.key_date = mbx_entry.issue_dt
                        'etl open and close panel description
                        etl_entry.tl_description = UCase("REM PANEL " & bow_entry.panel)
                        etl_entry.etl_entry_close_panel.tl_description = UCase("INST PANEL " & bow_entry.panel)

                        'etl's htl parent
                        etl_entry.parent_tl_ref_id = htl_entry.source_id
                        etl_entry.etl_entry_close_panel.parent_tl_ref_id = htl_entry.source_id

                        'operations and materials
                        'check if there is at least one ETL
                        If Not successfully_transformed OrElse bow_entry.etl_list.Count = 0 Then
                            If successfully_transformed Then
                                Dim Log As New CLogEntry
                                successfully_transformed = False
                                Log.line = bow_entry.line
                                Log.mat_id = bow_entry.panel
                                Log.code = bow_entry.panel
                                Log.column = "Panel"
                                Log.log_type = CLogEntry.LOG_TYPE_ERROR
                                Log.description = "ETL is missing on the panel " & bow_entry.panel
                                ctrl.logGlobalList.Add(Log)
                            End If
                        Else
                            'if one ETL read operations on the ETL
                            If bow_entry.etl_list.ElementAt(0).Value.operation_list.Count = 0 Then
                                Dim Log As New CLogEntry
                                success_transform_op = False
                                Log.line = bow_entry.line
                                Log.mat_id = bow_entry.panel
                                Log.code = bow_entry.panel
                                Log.column = "Panel"
                                Log.log_type = CLogEntry.LOG_TYPE_ERROR
                                Log.description = "Operation is missing on the panel " & bow_entry.panel
                                ctrl.logGlobalList.Add(Log)
                            Else
                                'fill open panel operations
                                Dim op_entry As New CTmplEntry
                                success_transform_op = fillOperation(masterConfObj, mbx_entry, op_entry, bow_entry.etl_list.ElementAt(0).Value.operation_list.ElementAt(0).Value, mainApplObj)
                                op_entry.op_control_key = ctrl.confController.defaultValueList("op_control_key_removed")
                                op_entry.op_text = UCase("Panel " & bow_entry.panel & " REMOVED")

                                If success_transform_op Then
                                    mbx_entry.etl_entry.etlOperationList.Add(op_entry)
                                    mbx_entry.etl_entry.etlMaterialOperationList.Add(op_entry)
                                    this_count = this_count + 1
                                    ctrl.operation_cnt = ctrl.operation_cnt + 1
                                End If

                                'clone attribue
                                Dim op_entry_clone As New CTmplEntry
                                For Each prop As String In ctrl.confController.tmplDataConfList.Keys
                                    Dim val As Object = CallByName(op_entry, prop, CallType.Get)
                                    CallByName(op_entry_clone, prop, CallType.Let, New Object() {val})
                                Next
                                'clone close panel's operation
                                If bow_entry.etl_list.Count > 1 AndAlso bow_entry.etl_list.ElementAt(1).Value.operation_list.Count > 0 Then
                                    op_entry_clone.op_work_value = bow_entry.etl_list.ElementAt(1).Value.operation_list.ElementAt(0).Value.hours
                                    If op_entry_clone.op_work_value = 0 Then
                                        op_entry_clone.op_work_value = 0.1
                                    End If
                                    op_entry_clone.op_text = bow_entry.etl_list.ElementAt(1).Value.operation_list.ElementAt(0).Value.description
                                    MMigrationRules.setOperationDescriptionRules(op_entry_clone)
                                End If
                                'operation control key
                                op_entry.op_control_key = ctrl.confController.defaultValueList("op_control_key")
                                op_entry_clone.op_text = UCase("Panel " & bow_entry.panel & " INSTALLED")
                                'add operation to ETL close
                                mbx_entry.etl_entry.etl_entry_close_panel.etlOperationList.Add(op_entry_clone)
                                mbx_entry.etl_entry.etl_entry_close_panel.etlMaterialOperationList.Add(op_entry_clone)
                                this_count = this_count + 1
                                ctrl.operation_cnt = ctrl.operation_cnt + 1

                                'fill open panel materials
                                If Not ctrl.ignore_material Then
                                    For Each mat As BOWEntry In bow_entry.etl_list.ElementAt(0).Value.operation_list.ElementAt(0).Value.material_list.Values
                                        'material
                                        Dim mat_entry As New CTmplEntry
                                        If ctrl.check_mat AndAlso Not migrated_material_list.Contains(mat.part_number) Then
                                            Dim Log As New CLogEntry
                                            ctrl.material_ignored_cnt = ctrl.material_ignored_cnt + 1
                                            Log.line = mat.line
                                            Log.mat_id = mat.htl_id
                                            Log.code = mat.code
                                            Log.column = "comp_mat"
                                            Log.log_type = CLogEntry.LOG_TYPE_WARNING
                                            Log.description = mat.part_number & " was not found in the list of materials"
                                            ctrl.logGlobalList.Add(Log)
                                        Else
                                            success_transform_mat = fillMaterial(masterConfObj, mbx_entry, mat_entry, mat)
                                            If success_transform_mat Then
                                                mbx_entry.etl_entry.etlMaterialList.Add(mat_entry)
                                                mbx_entry.etl_entry.etlMaterialOperationList.Add(mat_entry)
                                                this_count = this_count + 1
                                                ctrl.material_cnt = ctrl.material_cnt + 1
                                            End If
                                        End If
                                    Next
                                    'close panel
                                    If bow_entry.etl_list.Count > 1 Then
                                        For Each mat As BOWEntry In bow_entry.etl_list.ElementAt(1).Value.operation_list.ElementAt(0).Value.material_list.Values
                                            If ctrl.check_mat AndAlso Not migrated_material_list.Contains(mat.part_number) Then
                                                Dim Log As New CLogEntry
                                                ctrl.material_ignored_cnt = ctrl.material_ignored_cnt + 1
                                                Log.line = mat.line
                                                Log.mat_id = mat.htl_id
                                                Log.code = mat.code
                                                Log.column = "comp_mat"
                                                Log.log_type = CLogEntry.LOG_TYPE_WARNING
                                                Log.description = mat.part_number & " was not found in the list of materials"
                                                ctrl.logGlobalList.Add(Log)
                                            Else
                                                'material
                                                Dim mat_entry As New CTmplEntry
                                                success_transform_mat = fillMaterial(masterConfObj, mbx_entry, mat_entry, mat)
                                                If success_transform_mat Then
                                                    mbx_entry.etl_entry.etl_entry_close_panel.etlMaterialList.Add(mat_entry)
                                                    mbx_entry.etl_entry.etl_entry_close_panel.etlMaterialOperationList.Add(mat_entry)
                                                    this_count = this_count + 1
                                                    ctrl.material_cnt = ctrl.material_cnt + 1
                                                End If
                                            End If
                                        Next
                                    End If
                                End If
                            End If
                        End If

                        'add to list if transform has been successful and save logs if any
                        If successfully_transformed AndAlso success_transform_op AndAlso success_transform_mat Then
                            countTmplEntries = countTmplEntries + this_count
                            'handle related htl's etl items
                            Dim relatedList As New List(Of CTmplEntry)
                            'opem
                            Dim htl_related_etl_entry As New CTmplEntry
                            htl_related_etl_entry.m_type = CTmplEntry.M_TYPE_HTL
                            htl_related_etl_entry.child_tl_ref_id = etl_entry.source_id
                            relatedList.Add(htl_related_etl_entry)
                            'close
                            htl_related_etl_entry = New CTmplEntry
                            htl_related_etl_entry.m_type = CTmplEntry.M_TYPE_HTL
                            htl_related_etl_entry.child_tl_ref_id = etl_entry.etl_entry_close_panel.source_id
                            relatedList.Add(htl_related_etl_entry)

                            For Each itemEntry As CTmplEntry In relatedList
                                If (htl_entry.htlRelatedHtlList.Count + htl_entry.htlRelatedEtlList.Count) = 0 Then
                                    'use headers
                                    htl_entry.child_tl_ref_id = itemEntry.child_tl_ref_id
                                ElseIf htl_entry.htlParentHtlList.Count > (htl_entry.htlRelatedHtlList.Count + htl_entry.htlRelatedEtlList.Count) Then
                                    'use an already existing link
                                    htl_entry.htlAllRelatedObjList(htl_entry.htlRelatedHtlList.Count + htl_entry.htlRelatedEtlList.Count - 1).child_tl_ref_id = itemEntry.child_tl_ref_id
                                Else
                                    countTmplEntries = countTmplEntries + 1
                                    ctrl.htl_rel_cnt = ctrl.htl_rel_cnt + 1
                                    htl_entry.htlAllRelatedObjList.Add(itemEntry)
                                End If
                                htl_entry.htlRelatedEtlList.Add(itemEntry)
                            Next

                            mbx_entry.is_fully_loaded = True
                            'handle multi applicability
                            If masterConfObj.appl_list.Count > 1 Then
                                For i As Integer = 1 To masterConfObj.appl_list.Count - 1 Step 1
                                    Dim item As CAircraftApplicability = masterConfObj.appl_list(i)
                                    Dim mbx_entry_app As New CTmplEntry
                                    If Not String.Equals(masterConfObj.appl_list(i - 1).ac_manuf, item.ac_manuf) Then
                                        mbx_entry_app.ac_manufacturer = item.ac_manuf
                                    End If
                                    If Not String.Equals(masterConfObj.appl_list(i - 1).ac_type, item.ac_type) Then
                                        mbx_entry_app.ac_type = item.ac_type
                                    End If
                                    If Not String.Equals(masterConfObj.appl_list(i - 1).ac_model, item.ac_model) Then
                                        mbx_entry_app.ac_model = item.ac_model
                                    End If
                                    mbx_entry.mbx_ac_applicability_list.Add(mbx_entry_app)
                                    countTmplEntries = countTmplEntries + 1
                                Next
                            End If

                            countTmplEntries = countTmplEntries + 3 + masterConfObj.appl_list.Count
                            ctrl.mbx_doc_cnt = ctrl.mbx_doc_cnt + 1
                            ctrl.htl_cnt = ctrl.htl_cnt + 1
                            ctrl.etl_cnt = ctrl.etl_cnt + 1
                            'close panel
                            countTmplEntries = countTmplEntries + 1 + masterConfObj.appl_list.Count
                            ctrl.etl_cnt = ctrl.etl_cnt + 1
                            'add mbx to list
                            list_mbx_doc.Add(bow_entry.panel, mbx_entry)
                        Else
                            list_src_error.Add(bow_entry.panel)
                            ctrl.failed_cnt = ctrl.failed_cnt + this_count
                        End If
                    End If
                    'log
                    If (line) >= entriesInOneLog * (logCount + 1) Then
                        ctrl.addDetailsLog("Panels have been transformed: " & line & ", " & line_end - line & " are remaning")
                        logCount = logCount + 1
                        'increment progress 15% for read DB, 20 for organizing data, 30 for transforming, 30 for transposing, 5 for saving
                        ctrl.incrementSubProgressBar(1) '1*10=20
                    End If
                    line = line + 1
                Next

                ctrl.setSubProgressBar(45)

                For Each bow_entry In bow_entry_dict.Values
                    'flag to tell if the line from GQT has been processed with success
                    Dim successfully_transformed As Boolean = True
                    Dim success_transform_op As Boolean = True
                    Dim success_transform_mat As Boolean = True
                    Dim this_count As Integer = 0
                    Dim ruleResultStr As KeyValuePair(Of String, CLogEntry) = Nothing
                    'fill mbx
                    'check if the item has not yet been processed
                    If Not list_src_error.Contains(bow_entry.code_htl) Then
                        mbx_entry = New CTmplEntry
                        htl_entry = New CTmplEntry
                        etl_entry = New CTmplEntry
                        mbx_entry.htl_entry = htl_entry
                        mbx_entry.etl_entry = etl_entry

                        successfully_transformed = fillMBX(masterConfObj, mainApplObj, mbx_entry, bow_entry)
                        successfully_transformed = successfully_transformed AndAlso fillHTL(masterConfObj, mainApplObj, htl_entry, bow_entry)

                        If bow_entry.etl_list.Count = 0 Then
                            successfully_transformed = False
                            Dim Log As New CLogEntry
                            Log.line = bow_entry.line
                            Log.mat_id = bow_entry.htl_id
                            Log.code = bow_entry.code
                            Log.column = "etl_id"
                            Log.log_type = CLogEntry.LOG_TYPE_ERROR
                            Log.description = "There is no ETL found on the HTL " & bow_entry.htl_id
                            ctrl.logGlobalList.Add(Log)
                        ElseIf bow_entry.etl_list.Count > 1 Then
                            successfully_transformed = False
                            Dim Log As New CLogEntry
                            Log.line = bow_entry.line
                            Log.mat_id = bow_entry.htl_id
                            Log.code = bow_entry.code
                            Log.column = "etl_id"
                            Log.log_type = CLogEntry.LOG_TYPE_ERROR
                            Log.description = "More than one ETL was found on the HTL " & bow_entry.htl_id
                            ctrl.logGlobalList.Add(Log)
                        Else
                            'check if pm_ps is removed
                            Dim bow_etl As BOWEntry = bow_entry.etl_list.ElementAt(0).Value
                            'if pm_ps is remove, add ROUTINE as suffix to consider ROUTINE_REMOVE and no PANEL REMOVE
                            If Not String.IsNullOrWhiteSpace(bow_etl.pm_ps) AndAlso bow_etl.pm_ps.ToUpper = "REMOVE" Then
                                bow_etl.pm_ps = "REMOVE_ROUTINE"
                            End If
                            successfully_transformed = successfully_transformed AndAlso fillETL(masterConfObj, mainApplObj, etl_entry, bow_etl)
                        End If

                        htl_entry.key_date = mbx_entry.issue_dt
                        etl_entry.key_date = mbx_entry.issue_dt
                        'mbx des, lng desc
                        MMigrationRules.setMBXDocDescriptionRules(mbx_entry)
                        'link with htl mbx
                        htl_entry.dir_source_id = mbx_entry.source_id
                        mbx_entry.child_tl_ref_id = htl_entry.source_id
                        'etl's htl parent
                        etl_entry.parent_tl_ref_id = htl_entry.source_id
                        'operations and materials
                        'check if there is at least one ETL
                        If Not successfully_transformed OrElse bow_entry.etl_list.Count = 0 OrElse bow_entry.etl_list.ElementAt(0).Value.operation_list.Count = 0 Then
                            If successfully_transformed Then
                                Dim Log As New CLogEntry
                                successfully_transformed = False
                                Log.line = bow_entry.line
                                Log.mat_id = bow_entry.htl_id
                                Log.code = bow_entry.code
                                Log.column = "HTL"
                                Log.log_type = CLogEntry.LOG_TYPE_ERROR
                                Log.description = "ETL is missing on the htl " & bow_entry.htl_id
                                ctrl.logGlobalList.Add(Log)
                            End If
                        Else
                            For Each opBowEntry As BOWEntry In bow_entry.etl_list.ElementAt(0).Value.operation_list.Values
                                'fill open panel operations
                                Dim op_entry As New CTmplEntry
                                success_transform_op = fillOperation(masterConfObj, mbx_entry, op_entry, opBowEntry, mainApplObj)
                                op_entry.op_stand_id = ctrl.confController.defaultValueList("op_stand_id")
                                If success_transform_op Then
                                    mbx_entry.etl_entry.etlOperationList.Add(op_entry)
                                    mbx_entry.etl_entry.etlMaterialOperationList.Add(op_entry)
                                    this_count = this_count + 1
                                    ctrl.operation_cnt = ctrl.operation_cnt + 1
                                End If
                            Next
                            'fill materials
                            If Not ctrl.ignore_material Then
                                For Each ops As BOWEntry In bow_entry.etl_list.ElementAt(0).Value.operation_list.Values
                                    For Each mat As BOWEntry In ops.material_list.Values
                                        If ctrl.check_mat AndAlso Not migrated_material_list.Contains(mat.part_number) Then
                                            Dim Log As New CLogEntry
                                            ctrl.material_ignored_cnt = ctrl.material_ignored_cnt + 1
                                            Log.line = mat.line
                                            Log.mat_id = mat.htl_id
                                            Log.code = mat.code
                                            Log.column = "comp_mat"
                                            Log.log_type = CLogEntry.LOG_TYPE_WARNING
                                            Log.description = mat.part_number & " was not found in the list of materials"
                                            ctrl.logGlobalList.Add(Log)
                                        Else
                                            'material
                                            Dim mat_entry As New CTmplEntry
                                            success_transform_mat = fillMaterial(masterConfObj, mbx_entry, mat_entry, mat)
                                            If success_transform_mat Then
                                                mbx_entry.etl_entry.etlMaterialList.Add(mat_entry)
                                                mbx_entry.etl_entry.etlMaterialOperationList.Add(mat_entry)
                                                this_count = this_count + 1
                                                ctrl.material_cnt = ctrl.material_cnt + 1
                                            End If
                                        End If
                                    Next
                                Next
                            End If
                        End If

                        'add to list if transform has been successful and save logs if any
                        If successfully_transformed AndAlso success_transform_op AndAlso success_transform_mat Then
                            countTmplEntries = countTmplEntries + this_count
                            'handle related htl's etl items and htl's panel
                            'ETL
                            Dim relatedList As New List(Of CTmplEntry)
                            Dim htl_related_entry As New CTmplEntry
                            Dim panelRelatedList As New List(Of CTmplEntry)
                            htl_related_entry.m_type = CTmplEntry.M_TYPE_HTL
                            htl_related_entry.child_tl_ref_id = etl_entry.source_id
                            relatedList.Add(htl_related_entry)
                            'PANEL
                            For Each my_panel As String In bow_entry.panel_list.Keys
                                If list_mbx_doc.ContainsKey(my_panel) Then
                                    htl_related_entry = New CTmplEntry
                                    htl_related_entry.m_type = CTmplEntry.M_TYPE_HTL
                                    htl_related_entry.child_tl_ref_id = list_mbx_doc(my_panel).htl_entry.source_id
                                    relatedList.Add(htl_related_entry)
                                    'add as parent on panel
                                    htl_related_entry = New CTmplEntry
                                    htl_related_entry.m_type = CTmplEntry.M_TYPE_HTL
                                    htl_related_entry.parent_tl_ref_id = htl_entry.source_id
                                    htl_related_entry.external_ref_no = my_panel
                                    panelRelatedList.Add(htl_related_entry)
                                End If
                            Next
                            'link htl to related panel's htl
                            For Each item As CTmplEntry In relatedList
                                If (htl_entry.htlRelatedHtlList.Count + htl_entry.htlRelatedEtlList.Count) = 0 Then
                                    'use headers
                                    htl_entry.child_tl_ref_id = item.child_tl_ref_id
                                ElseIf htl_entry.htlParentHtlList.Count > (htl_entry.htlRelatedHtlList.Count + htl_entry.htlRelatedEtlList.Count) Then
                                    'use an already existing link
                                    htl_entry.htlAllRelatedObjList(htl_entry.htlRelatedHtlList.Count + htl_entry.htlRelatedEtlList.Count - 1).child_tl_ref_id = item.child_tl_ref_id
                                Else
                                    countTmplEntries = countTmplEntries + 1
                                    ctrl.htl_rel_cnt = ctrl.htl_rel_cnt + 1
                                    htl_entry.htlAllRelatedObjList.Add(item)
                                End If
                                htl_entry.htlRelatedHtlList.Add(item.child_tl_ref_id, item)
                            Next
                            'link panel with htl parent
                            For Each item As CTmplEntry In panelRelatedList
                                Dim my_htl_entry As CTmplEntry = list_mbx_doc(item.external_ref_no).htl_entry
                                If my_htl_entry.htlParentHtlList.Count = 0 Then
                                    'use headers
                                    my_htl_entry.parent_tl_ref_id = htl_entry.source_id
                                ElseIf my_htl_entry.htlParentHtlList.Count < (my_htl_entry.htlRelatedHtlList.Count + my_htl_entry.htlRelatedEtlList.Count) Then
                                    'use an already existing link
                                    my_htl_entry.htlAllRelatedObjList(my_htl_entry.htlParentHtlList.Count - 1).parent_tl_ref_id = htl_entry.source_id
                                Else
                                    countTmplEntries = countTmplEntries + 1
                                    ctrl.htl_rel_cnt = ctrl.htl_rel_cnt + 1
                                    my_htl_entry.htlAllRelatedObjList.Add(item)
                                End If
                                my_htl_entry.htlParentHtlList.Add(item)
                                item.external_ref_no = ""
                            Next

                            mbx_entry.is_fully_loaded = True
                            'handle multi applicability
                            If masterConfObj.appl_list.Count > 1 Then
                                For i As Integer = 1 To masterConfObj.appl_list.Count - 1 Step 1
                                    Dim item As CAircraftApplicability = masterConfObj.appl_list(i)
                                    Dim mbx_entry_app As New CTmplEntry
                                    If Not String.Equals(masterConfObj.appl_list(i - 1).ac_manuf, item.ac_manuf) Then
                                        mbx_entry_app.ac_manufacturer = item.ac_manuf
                                    End If
                                    If Not String.Equals(masterConfObj.appl_list(i - 1).ac_type, item.ac_type) Then
                                        mbx_entry_app.ac_type = item.ac_type
                                    End If
                                    If Not String.Equals(masterConfObj.appl_list(i - 1).ac_model, item.ac_model) Then
                                        mbx_entry_app.ac_model = item.ac_model
                                    End If
                                    mbx_entry.mbx_ac_applicability_list.Add(mbx_entry_app)
                                    countTmplEntries = countTmplEntries + 1
                                Next
                            End If

                            mbx_entry.htl_entry = htl_entry
                            mbx_entry.etl_entry = etl_entry
                            countTmplEntries = countTmplEntries + 3 + masterConfObj.appl_list.Count
                            ctrl.mbx_doc_cnt = ctrl.mbx_doc_cnt + 1
                            ctrl.htl_cnt = ctrl.htl_cnt + 1
                            ctrl.etl_cnt = ctrl.etl_cnt + 1
                            'add mbx to list
                            list_mbx_doc.Add(bow_entry.code_htl, mbx_entry)
                        Else
                            list_src_error.Add(bow_entry.code_htl)
                            ctrl.failed_cnt = ctrl.failed_cnt + this_count
                        End If
                    End If
                    'log
                    If (line) >= entriesInOneLog * (logCount + 1) Then
                        ctrl.addDetailsLog("Panels have been transformed: " & line & ", " & line_end - line & " are remaning")
                        logCount = logCount + 1
                        'increment progress 15% for read DB, 20 for organizing data, 30 for transforming, 30 for transposing, 5 for saving
                        ctrl.incrementSubProgressBar(2) '1*10=20
                    End If
                    line = line + 1
                Next
            Catch ex As Exception
                Throw New Exception("An error occured while loading and transforming the data source" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
            Finally
                dt.Clear()
                MUtils.releaseObject(wb_sht)
                MUtils.releaseObject(wb)
            End Try
            ctrl.setSubProgressBar(65)
            'write in output file
            writeTransformedData(masterConfObj)
            ctrl.setSubProgressBar(100)
            'log
            If Master_line >= Master_entriesInOneLog * (Master_logCount + 1) Then
                ctrl.addDetailsLog("A/C entries read from the database: " & Master_line & ", Number of remaining items : " & ctrl.confController.masterList.Count - Master_line)
                Master_logCount = Master_logCount + 1
                'increment progress 80% all entries in master conf file.
                ctrl.incrementProgressBar(Master_Increment)
            End If
            Master_line = Master_line + 1
        Next

    End Sub

    Public Function fillMBX(masterConfObj As CMasterConfObj, mainApplObj As CAircraftApplicability, mbx_entry As CTmplEntry, bow_entry As BOWEntry) As Boolean
        Dim ruleResultStr As KeyValuePair(Of String, CLogEntry) = Nothing
        Dim successfully_transformed = True
        mbx_entry.mbx_ac_applicability_list = New List(Of CTmplEntry)
        mbx_entry.gqt_entry = bow_entry
        'm_type 
        ruleResultStr = MMigrationRules.getTmplMType(bow_entry)
        If IsNothing(ruleResultStr.Value) Then
            mbx_entry.m_type = ruleResultStr.Key
        Else
            'log error
            successfully_transformed = False
            ctrl.logGlobalList.Add(ruleResultStr.Value)
        End If
        'mbx
        mbx_entry.source_id = ctrl.confController.defaultValueList("mbx_doc_src_id_pref") & "_" & masterConfObj.src_id_ac_type_text & "_" & getBOWEntryVirtualCode(bow_entry)
        mbx_entry.source_id = Replace(mbx_entry.source_id, ".", "")
        If Len(mbx_entry.source_id) > 40 Then
            Dim Log As New CLogEntry
            Log.line = bow_entry.line
            Log.mat_id = bow_entry.panel
            Log.code = bow_entry.panel
            Log.column = "Panel"
            Log.log_type = CLogEntry.LOG_TYPE_ERROR
            Log.description = "Source_id has more than 40 characters: " & mbx_entry.source_id
            ctrl.logGlobalList.Add(Log)
        End If
        If mbx_entry.source_id.IndexOfAny("[~`!@#$%^&*()=|{}':;.,<>/?]+".ToCharArray) <> -1 Then
            Dim Log As New CLogEntry
            Log.line = bow_entry.line
            Log.mat_id = bow_entry.htl_id
            Log.code = bow_entry.code
            Log.column = "source_id"
            Log.log_type = CLogEntry.LOG_TYPE_ERROR
            Log.description = "Source_id contains special characters: " & mbx_entry.source_id
            ctrl.logGlobalList.Add(Log)
        End If
        'sequence
        mbx_entry.seq_id = 1
        'mbx des, lng desc
        mbx_entry.description = bow_entry.description
        MMigrationRules.setMBXDocDescriptionRules(mbx_entry)
        'mbx external ref
        mbx_entry.external_ref_no = bow_entry.code
        'ata code
        mbx_entry.ata = bow_entry.ata_chapter
        'mbx doc repeat application
        mbx_entry.repeat_application = ctrl.confController.defaultValueList("repeat_application")
        'mbx doc engineering dept
        mbx_entry.engineering_dept = masterConfObj.engineering_dept
        'mbx doc planning plant
        mbx_entry.pplant = ctrl.confController.defaultValueList("pplant")
        'mbx doc issue date
        mbx_entry.issue_dt = masterConfObj.issue_dt
        'mbx doc ac type
        mbx_entry.ac_type = mainApplObj.ac_type
        'mbx doc ac manufacturer
        mbx_entry.ac_manufacturer = mainApplObj.ac_manuf
        'mbx doc ac model
        mbx_entry.ac_model = mainApplObj.ac_model
        'mbx doc OEM JA code
        mbx_entry.oem_ja_code = bow_entry.code
        'mbx doc serv mat
        mbx_entry.serv_material = ctrl.confController.defaultValueList("serv_material")
        'oem doc data
        mbx_entry.oem_doc_ref = bow_entry.src_ref
        mbx_entry.oem_doc_rev = bow_entry.src_rev
        mbx_entry.oem_doc_type = bow_entry.src_type
        Return successfully_transformed
    End Function


    Public Function fillHTL(masterConfObj As CMasterConfObj, mainApplObj As CAircraftApplicability, htl_entry As CTmplEntry, bow_entry As BOWEntry) As Boolean
        htl_entry.seq_id = 1
        htl_entry.tl_type = ctrl.confController.defaultValueList("tl_type")
        htl_entry.plant = ctrl.confController.defaultValueList("plant")
        htl_entry.tl_ext_id = bow_entry.code
        htl_entry.status = ctrl.confController.defaultValueList("status")
        htl_entry.planner_group = masterConfObj.engineering_dept
        htl_entry.usage = ctrl.confController.defaultValueList("usage")
        htl_entry.tl_description = bow_entry.description
        htl_entry.htlRelatedHtlList = New Dictionary(Of String, CTmplEntry)
        htl_entry.htlParentHtlList = New List(Of CTmplEntry)
        htl_entry.htlRelatedEtlList = New List(Of CTmplEntry)
        htl_entry.htlAllRelatedObjList = New List(Of CTmplEntry)
        htl_entry.m_type = CTmplEntry.M_TYPE_HTL
        'htl
        htl_entry.source_id = htl_entry.m_type & "_" & masterConfObj.src_id_ac_type_text & "_" & getBOWEntryVirtualCode(bow_entry)
        If Len(htl_entry.source_id) > 40 Then
            Dim Log As New CLogEntry
            Log.line = bow_entry.line
            Log.mat_id = bow_entry.panel
            Log.code = bow_entry.panel
            Log.column = "Panel"
            Log.log_type = CLogEntry.LOG_TYPE_ERROR
            Log.description = "Source_id has more than 40 characters: " & htl_entry.source_id
            ctrl.logGlobalList.Add(Log)
        End If
        Return True
    End Function

    Public Function fillETL(masterConfObj As CMasterConfObj, mainApplObj As CAircraftApplicability, etl_entry As CTmplEntry, bow_entry As BOWEntry) As Boolean
        Dim success_transform As Boolean = True
        Dim ruleResultStr As KeyValuePair(Of String, CLogEntry) = Nothing
        etl_entry.seq_id = 1
        etl_entry.etlOperationList = New List(Of CTmplEntry)
        etl_entry.etlMaterialList = New List(Of CTmplEntry)
        etl_entry.etlMaterialOperationList = New List(Of CTmplEntry)
        etl_entry.m_type = CTmplEntry.M_TYPE_ETL
        etl_entry.source_id = etl_entry.m_type & "_" & masterConfObj.src_id_ac_type_text & "_" & getBOWEntryVirtualCode(bow_entry)
        If Len(etl_entry.source_id) > 40 Then
            Dim Log As New CLogEntry
            Log.line = bow_entry.line
            Log.mat_id = bow_entry.htl_id
            Log.code = bow_entry.code
            Log.column = "External Reference"
            Log.log_type = CLogEntry.LOG_TYPE_ERROR
            Log.description = "Source_id has more than 40 characters: " & etl_entry.source_id
            ctrl.logGlobalList.Add(Log)
        End If
        etl_entry.seq_id = 1
        etl_entry.tl_type = ctrl.confController.defaultValueList("tl_type")
        etl_entry.tl_description = bow_entry.description
        etl_entry.plant = ctrl.confController.defaultValueList("plant")
        etl_entry.tl_ext_id = bow_entry.code
        etl_entry.status = ctrl.confController.defaultValueList("status")
        etl_entry.planner_group = masterConfObj.engineering_dept
        ruleResultStr = MMigrationRules.getPMPSReference(bow_entry)
        If IsNothing(ruleResultStr.Value) Then
            etl_entry.pm_ps = ruleResultStr.Key
        Else
            'log error
            success_transform = False
            ctrl.logGlobalList.Add(ruleResultStr.Value)
        End If
        etl_entry.usage = ctrl.confController.defaultValueList("usage")
        etl_entry.work_center = ctrl.confController.defaultValueList("etl_work_center")
        Return success_transform
    End Function

    Public Function fillOperation(masterConfObj As CMasterConfObj, mbx_entry As CTmplEntry, op_entry As CTmplEntry, bow_entry As BOWEntry, mainApplObj As CAircraftApplicability) As Boolean
        Dim success_transform_op As Boolean = True
        Dim ruleResultStr As KeyValuePair(Of String, CLogEntry) = Nothing
        'type
        op_entry.m_type = CTmplEntry.M_TYPE_ETL
        'operation number
        op_entry.operation_no = ((mbx_entry.etl_entry.etlOperationList.Count + 1) * 10).ToString("D4")
        'operation plant
        op_entry.op_plant = mbx_entry.etl_entry.plant
        'operation work center
        ruleResultStr = MMigrationRules.getWorkCenter(bow_entry, mainApplObj)
        If IsNothing(ruleResultStr.Value) Then
            op_entry.op_work_center = ruleResultStr.Key
        Else
            'log error
            success_transform_op = False
            ctrl.logGlobalList.Add(ruleResultStr.Value)
        End If
        'operation control key
        op_entry.op_control_key = ctrl.confController.defaultValueList("op_control_key")
        'work value
        Dim work_value As Double
        If Double.TryParse(bow_entry.hours, work_value) Then
            op_entry.op_work_value = Math.Round(work_value, 1)
        Else
            success_transform_op = False
            Dim Log As New CLogEntry
            Log.line = bow_entry.line
            Log.mat_id = bow_entry.panel
            Log.code = bow_entry.code
            Log.column = "Hours, col " & ctrl.confController.srcDataConfList("hours")
            Log.log_type = CLogEntry.LOG_TYPE_ERROR
            Log.description = "operation hours " & bow_entry.hours & ", is not a numeric value"
            ctrl.logGlobalList.Add(Log)
        End If
        'work unit
        op_entry.op_work_unit = "H"
        'avoid 0
        If op_entry.op_work_value = 0 Then
            op_entry.op_work_value = 0.1
        End If
        'calculate effort
        op_entry.op_calculate_effort = ctrl.confController.defaultValueList("op_calculate_effort")
        op_entry.op_text = bow_entry.description
        MMigrationRules.setOperationDescriptionRules(op_entry)
        Return success_transform_op
    End Function

    Public Function fillMaterial(masterConfObj As CMasterConfObj, mbx_entry As CTmplEntry, mat_entry As CTmplEntry, bow_entry As BOWEntry) As Boolean
        Dim success_transform_mat As Boolean = True
        'type
        mat_entry.m_type = CTmplEntry.M_TYPE_ETL
        'operation number
        mat_entry.mat_operation_no = "0010" 'bow_entry.operation

        'material
        mat_entry.mat_comp_mat = bow_entry.part_number

        If String.IsNullOrWhiteSpace(mat_entry.mat_comp_mat) Then
            Dim Log As New CLogEntry
            Log.line = bow_entry.line
            Log.mat_id = bow_entry.htl_id
            Log.code = bow_entry.code
            Log.column = "mat_comp_mat, part number col"
            Log.log_type = CLogEntry.LOG_TYPE_ERROR
            Log.description = "Material PN must not be empty"
            ctrl.logGlobalList.Add(Log)
        End If
        'check special characters
        If mat_entry.mat_comp_mat.IndexOfAny("[~`!@#$%^&*=|{}':;,<>?]+".ToCharArray) <> -1 Then
            Dim Log As New CLogEntry
            Log.line = bow_entry.line
            Log.mat_id = bow_entry.htl_id
            Log.code = bow_entry.code
            Log.column = "comp_mat"
            Log.log_type = CLogEntry.LOG_TYPE_ERROR
            Log.description = "Material PN contains special characters: " & mat_entry.mat_comp_mat
            ctrl.logGlobalList.Add(Log)
        End If
        'check PN lenght
        If Not String.IsNullOrWhiteSpace(mat_entry.mat_comp_mat) And mat_entry.mat_comp_mat.Length > 40 Then
            Dim Log As New CLogEntry
            Log.line = bow_entry.line
            Log.mat_id = bow_entry.htl_id
            Log.code = bow_entry.code
            Log.column = "comp_mat"
            Log.log_type = CLogEntry.LOG_TYPE_ERROR
            Log.description = "Material PN contains more than 40 characters " & mat_entry.mat_comp_mat
            ctrl.logGlobalList.Add(Log)
        End If

        'material  qty
        Dim mat_qty As Double
        If Double.TryParse(bow_entry.qty, mat_qty) Then
            mat_entry.mat_comp_qty = mat_qty
        Else
            success_transform_mat = False
            Dim Log As New CLogEntry
            Log.line = bow_entry.line
            Log.mat_id = bow_entry.htl_id
            Log.code = bow_entry.code
            Log.column = "Qty, col " & ctrl.confController.srcDataConfList("qty")
            Log.log_type = CLogEntry.LOG_TYPE_ERROR
            Log.description = "Material qty " & bow_entry.qty & ", is not a numeric value"
            ctrl.logGlobalList.Add(Log)
        End If
        Return success_transform_mat
    End Function

    Public Sub writeTransformedData(masterConfObj As CMasterConfObj)
        'init worbook and worksheet to read GQT
        Dim wb As Microsoft.Office.Interop.Excel.Workbook = Nothing
        Dim wb_sht As Microsoft.Office.Interop.Excel.Worksheet = Nothing
        Dim logWsheet As Microsoft.Office.Interop.Excel.Worksheet = Nothing

        Try
            Dim line_start As Integer = 0
            Dim col_start As String = ""
            Dim line_end As Integer = 0
            Dim firstLoop As Boolean = True
            'init tmpl entries
            countTmplEntries = countTmplEntries
            'log
            Dim MaxNumberOfLogs = 10
            Dim entriesInOneLog As Double = (countTmplEntries) / MaxNumberOfLogs
            Dim logCount As Long = 0

            'block calculation
            ctrl.xlFileHandler.OffCalc(ctrl.xlFileHandler.xlappUser)
            If countTmplEntries = 0 Then
                MsgBox("No data to fill in the template")
                Exit Sub
            End If
            'check if wb exists
            If Not IO.File.Exists(masterConfObj.task_list_tmpl_file) Then
                Throw New Exception("The template file " & masterConfObj.task_list_tmpl_file & " does not exists")
            End If
            wb = ctrl.xlFileHandler.openxlFile(masterConfObj.task_list_tmpl_file)
            'get worksheet
            wb_sht = wb.Worksheets(masterConfObj.task_list_tmpl_tab)
            If IsNothing(wb_sht) Then
                Throw New Exception("The tab " & masterConfObj.task_list_tmpl_tab & " does not exists in the source file " & masterConfObj.task_list_tmpl_file)
            End If
            'get line start
            If Not Integer.TryParse(masterConfObj.task_list_tmpl_line_start, line_start) Then
                Throw New Exception("The start line value of the template :" & masterConfObj.task_list_tmpl_line_start & " is not an Integer")
            End If
            'get col start
            col_start = masterConfObj.task_list_tmpl_col_start
            'clear sheet
            Dim xlrg_delete As Microsoft.Office.Interop.Excel.Range = wb_sht.Range(wb_sht.Cells(line_start, col_start), wb_sht.Cells(wb_sht.Rows.Count, col_start))
            'xlrg_delete.EntireRow.Delete()
            xlrg_delete.EntireRow.Clear()
            'array
            Dim arrayTmplColSize As Integer = ctrl.confController.tmplDataConfListReverse.Keys(ctrl.confController.tmplDataConfListReverse.Count - 1) - 1
            Dim arrayTmpl(countTmplEntries - 1, arrayTmplColSize) As String
            Dim i As Integer = 0

            For Each tmplE As CTmplEntry In list_mbx_doc.Values
                'handle characteristics on ETL
                tmplE.etl_entry.etlApplList = New List(Of CTmplEntry)
                For app_counter As Integer = 0 To masterConfObj.appl_list.Count - 1 Step 1
                    Dim appEntry As New CTmplEntry
                    If app_counter = 0 OrElse (Not String.Equals(masterConfObj.appl_list(app_counter - 1).ac_manuf, masterConfObj.appl_list(app_counter).ac_manuf)) Then
                        appEntry.tlc_ac_manuf = masterConfObj.appl_list(app_counter).ac_manuf
                    End If
                    If app_counter = 0 OrElse (Not String.Equals(masterConfObj.appl_list(app_counter - 1).ac_type, masterConfObj.appl_list(app_counter).ac_type)) Then
                        appEntry.tlc_ac_type = masterConfObj.appl_list(app_counter).ac_type
                    End If
                    If app_counter = 0 OrElse (Not String.Equals(masterConfObj.appl_list(app_counter - 1).ac_model, masterConfObj.appl_list(app_counter).ac_model)) Then
                        appEntry.tlc_ac_model = masterConfObj.appl_list(app_counter).ac_model
                    End If
                    If tmplE.etl_entry.is_panel Then
                        appEntry.tlc_tl_task_type = "PANEL"
                        appEntry.tlc_tl_panel_code = tmplE.external_ref_no
                        appEntry.tlc_tl_ac_zone = tmplE.etl_entry.gqt_entry.zone
                        appEntry.tlc_tl_critical_task = "NO"
                        'check panel and zones
                        If String.IsNullOrWhiteSpace(appEntry.tlc_tl_panel_code) OrElse appEntry.tlc_tl_panel_code.Length > 8 Then
                            Dim Log As New CLogEntry
                            Log.line = tmplE.gqt_entry.line
                            Log.mat_id = tmplE.gqt_entry.etl_id
                            Log.code = tmplE.gqt_entry.code
                            Log.column = "Code"
                            Log.log_type = CLogEntry.LOG_TYPE_ERROR
                            Log.description = "The Panel characteristics " & appEntry.tlc_tl_panel_code & " must not exceed 8 characters"
                            ctrl.logGlobalList.Add(Log)
                        End If
                        If String.IsNullOrWhiteSpace(appEntry.tlc_tl_ac_zone) OrElse appEntry.tlc_tl_ac_zone.Length > 8 Then
                            Dim Log As New CLogEntry
                            Log.line = tmplE.gqt_entry.line
                            Log.mat_id = tmplE.gqt_entry.etl_id
                            Log.code = tmplE.gqt_entry.code
                            Log.column = "Code"
                            Log.log_type = CLogEntry.LOG_TYPE_ERROR
                            Log.description = "The zone characteristics " & appEntry.tlc_tl_ac_zone & " must not exceed 3 characters"
                            ctrl.logGlobalList.Add(Log)
                        End If
                    Else
                        appEntry.tlc_tl_task_type = "MAIN"
                        appEntry.tlc_tl_panel_code = ""
                        appEntry.tlc_tl_ac_zone = ""
                        appEntry.tlc_tl_critical_task = "NO"
                    End If

                    tmplE.etl_entry.etlApplList.Add(appEntry)
                Next

                If tmplE.etl_entry.is_panel Then
                    'applicability clone
                    tmplE.etl_entry.etl_entry_close_panel.etlApplList = New List(Of CTmplEntry)
                    For Each appItem In tmplE.etl_entry.etlApplList
                        'clone op/mat
                        Dim appItem_close As New CTmplEntry
                        For Each prop As String In ctrl.confController.tmplDataConfList.Keys
                            Dim val As Object = CallByName(appItem, prop, CallType.Get)
                            CallByName(appItem_close, prop, CallType.Let, New Object() {val})
                        Next
                        tmplE.etl_entry.etl_entry_close_panel.etlApplList.Add(appItem_close)
                    Next
                End If

                'any material should be linked to at least one operation
                If tmplE.etl_entry.etlMaterialList.Count > 0 AndAlso tmplE.etl_entry.etlOperationList.Count = 0 Then
                    Dim Log As New CLogEntry
                    Log.line = tmplE.gqt_entry.line
                    Log.mat_id = tmplE.gqt_entry.htl_id
                    Log.code = tmplE.gqt_entry.code
                    Log.column = "Labor"
                    Log.log_type = CLogEntry.LOG_TYPE_ERROR
                    Log.description = "This ETL has material where as there is no operation linked to it"
                    ctrl.logGlobalList.Add(Log)
                End If

                'setPMPS Reference
                MMigrationRules.setPanelPMPSReference(tmplE.etl_entry)

                'write
                'mbx
                tmplE.child_tl_ref_id = tmplE.htl_entry.source_id
                fillArray(tmplE, i, arrayTmpl)
                i = i + 1
                'multiple applicability
                Dim sequence As Integer = 2
                For Each subAppl As CTmplEntry In tmplE.mbx_ac_applicability_list
                    subAppl.source_id = tmplE.source_id
                    subAppl.m_type = tmplE.m_type
                    subAppl.child = ctrl.confController.defaultValueList("child")
                    subAppl.seq_id = sequence
                    fillArray(subAppl, i, arrayTmpl)
                    i = i + 1
                    sequence = sequence + 1
                Next

                'htl
                fillArray(tmplE.htl_entry, i, arrayTmpl)
                i = i + 1
                'etl
                tmplE.etl_entry.parent_tl_ref_id = tmplE.htl_entry.source_id
                fillArray(tmplE.etl_entry, i, arrayTmpl)
                i = i + 1
                'check PMPS reference for logs
                If String.IsNullOrWhiteSpace(tmplE.etl_entry.pm_ps) Then
                    Dim Log As New CLogEntry
                    Log.line = tmplE.gqt_entry.line
                    Log.mat_id = tmplE.gqt_entry.htl_id
                    Log.code = tmplE.gqt_entry.code
                    Log.column = "PM/PS"
                    Log.log_type = CLogEntry.LOG_TYPE_ERROR
                    Log.description = "The PM/PS Reference field is empty"
                    ctrl.logGlobalList.Add(Log)
                End If

                'etl operation and material
                sequence = 2
                For Each tmplSubE As CTmplEntry In tmplE.etl_entry.etlMaterialOperationList
                    'sequence
                    tmplSubE.seq_id = sequence
                    tmplSubE.m_type = tmplE.etl_entry.m_type
                    tmplSubE.child = ctrl.confController.defaultValueList("child")
                    tmplSubE.source_id = tmplE.etl_entry.source_id
                    fillArray(tmplSubE, i, arrayTmpl)
                    i = i + 1
                    sequence = sequence + 1
                Next
                'applicability entries (sequence is not reset to 2)
                If Not IsNothing(tmplE.etl_entry.etlApplList) Then
                    For Each tmplSubE As CTmplEntry In tmplE.etl_entry.etlApplList
                        'sequence
                        tmplSubE.seq_id = sequence
                        tmplSubE.m_type = tmplE.etl_entry.m_type
                        tmplSubE.child = ctrl.confController.defaultValueList("child")
                        tmplSubE.source_id = tmplE.etl_entry.source_id
                        fillArray(tmplSubE, i, arrayTmpl)
                        i = i + 1
                        sequence = sequence + 1
                    Next
                End If

                'if panel
                If tmplE.etl_entry.is_panel Then
                    'etl closed
                    tmplE.etl_entry.etl_entry_close_panel.parent_tl_ref_id = tmplE.htl_entry.source_id
                    fillArray(tmplE.etl_entry.etl_entry_close_panel, i, arrayTmpl)
                    i = i + 1

                    'etl operation and material
                    sequence = 2
                    For Each tmplSubE As CTmplEntry In tmplE.etl_entry.etl_entry_close_panel.etlMaterialOperationList
                        'sequence
                        tmplSubE.seq_id = sequence
                        tmplSubE.child = ctrl.confController.defaultValueList("child")
                        tmplSubE.source_id = tmplE.etl_entry.etl_entry_close_panel.source_id
                        fillArray(tmplSubE, i, arrayTmpl)
                        i = i + 1
                        sequence = sequence + 1
                    Next
                    'applicability entries (sequence is not reset to 2)
                    If Not IsNothing(tmplE.etl_entry.etl_entry_close_panel.etlApplList) Then
                        For Each tmplSubE As CTmplEntry In tmplE.etl_entry.etl_entry_close_panel.etlApplList
                            'sequence
                            tmplSubE.seq_id = sequence
                            tmplSubE.m_type = tmplE.etl_entry.etl_entry_close_panel.m_type
                            tmplSubE.child = ctrl.confController.defaultValueList("child")
                            tmplSubE.source_id = tmplE.etl_entry.etl_entry_close_panel.source_id
                            fillArray(tmplSubE, i, arrayTmpl)
                            i = i + 1
                            sequence = sequence + 1
                        Next
                    End If
                End If


                'htl related chidlren (htl or etl) and parent (htl) items
                sequence = 2

                For Each tmplSubE As CTmplEntry In tmplE.htl_entry.htlAllRelatedObjList
                    tmplSubE.source_id = tmplE.htl_entry.source_id
                    tmplSubE.child = ctrl.confController.defaultValueList("child")
                    tmplSubE.seq_id = sequence
                    fillArray(tmplSubE, i, arrayTmpl)
                    i = i + 1
                    sequence = sequence + 1
                Next

                'log
                If (i + 1) >= entriesInOneLog * (logCount + 1) Then
                    ctrl.addDetailsLog(" " & i & " template lines have been post-transformed and transposed, " & countTmplEntries - i & " are remaning")
                    logCount = logCount + 1
                    'increment progress 15% for read DB, 20 for organizing data, 30 for transforming, 30 for transposing, 5 for saving
                    ctrl.incrementSubProgressBar(3)
                End If
            Next
            'write in template
            ctrl.addDetailsLog("Paste data in the template file")
            Dim xlrg As Microsoft.Office.Interop.Excel.Range = CType(wb_sht.Cells(line_start, col_start), Microsoft.Office.Interop.Excel.Range)
            xlrg = wb_sht.Range(xlrg, xlrg.Offset(countTmplEntries - 1, arrayTmplColSize))
            xlrg.Value = arrayTmpl
            ctrl.incrementSubProgressBar(5)

            'handle conditional formating
            'get m_type col and sequence id col
            ctrl.addDetailsLog("Apply conditional formatting")
            Dim colStartLng As Integer = wb_sht.Range(col_start & 1).Column
            Dim m_type_col As String = Split(wb_sht.Cells(1, CLng(ctrl.confController.tmplDataConfList("m_type")) + colStartLng - 1).Address, "$")(1)
            Dim seq_id_col As String = Split(wb_sht.Cells(1, CLng(ctrl.confController.tmplDataConfList("seq_id")) + colStartLng - 1).Address, "$")(1)
            Dim op_number_col As String = Split(wb_sht.Cells(1, CLng(ctrl.confController.tmplDataConfList("operation_no")) + colStartLng - 1).Address, "$")(1)
            Dim mat_number_col As String = Split(wb_sht.Cells(1, CLng(ctrl.confController.tmplDataConfList("mat_comp_mat")) + colStartLng - 1).Address, "$")(1)
            Dim formatCond As FormatCondition = Nothing
            Dim formulaS As String = ""
            'MBX Format
            formulaS = "=AND($" & m_type_col & line_start & "<>" & """" & CTmplEntry.M_TYPE_HTL & """" & ";" & "$" & m_type_col & line_start & "<>" & """" & CTmplEntry.M_TYPE_ETL & """)"
            formatCond = xlrg.FormatConditions.Add(XlFormatConditionType.xlExpression,, formulaS)
            formatCond.SetFirstPriority()
            formatCond.StopIfTrue = False
            With formatCond.Interior
                .Color = 49407
                .TintAndShade = 0
            End With
            'HTL Format
            formulaS = "=$" & m_type_col & line_start & "=" & """" & CTmplEntry.M_TYPE_HTL & """"
            formatCond = xlrg.FormatConditions.Add(XlFormatConditionType.xlExpression,, formulaS)
            formatCond.SetFirstPriority()
            formatCond.StopIfTrue = False
            With formatCond.Interior
                .Color = 15917529
                .TintAndShade = 0
            End With
            'ETL Format
            formulaS = "=AND($" & m_type_col & line_start & "=" & """" & CTmplEntry.M_TYPE_ETL & """" & ";" & "$" & op_number_col & line_start & "=" & """""" & ";" & "$" & mat_number_col & line_start & "=" & """"")"
            formatCond = xlrg.FormatConditions.Add(XlFormatConditionType.xlExpression,, formulaS)
            formatCond.SetFirstPriority()
            formatCond.StopIfTrue = False
            With formatCond.Interior
                .Color = 11389944
                .TintAndShade = 0
            End With
            'ETL Operation & Material Format
            formulaS = "=AND($" & m_type_col & line_start & "=" & """" & CTmplEntry.M_TYPE_ETL & """" & "; OR(" & "$" & op_number_col & line_start & "<>" & """"";$" & mat_number_col & line_start & "<>" & """""))"
            formatCond = xlrg.FormatConditions.Add(XlFormatConditionType.xlExpression,, formulaS)
            formatCond.SetFirstPriority()
            formatCond.StopIfTrue = False
            With formatCond.Interior
                .Color = 13431551
                .TintAndShade = 0
            End With
            'ETL characteristics
            formulaS = "=AND($" & m_type_col & line_start & "=" & """" & CTmplEntry.M_TYPE_ETL & """" & "; " & "$" & op_number_col & line_start & "=" & """"";$" & mat_number_col & line_start & "=" & """"";$" & seq_id_col & line_start & "<>" & """1"")"
            formatCond = xlrg.FormatConditions.Add(XlFormatConditionType.xlExpression,, formulaS)
            formatCond.SetFirstPriority()
            formatCond.StopIfTrue = False
            With formatCond.Interior
                .Color = 52377
                .TintAndShade = 0
            End With
            ctrl.addDetailsLog("Write logs...")
            ctrl.logs_cnt = ctrl.logGlobalList.Count
            MMigrationRules.addStatisticsToLogs()
            'write logs
            Try
                logWsheet = wb.Worksheets(masterConfObj.log_table_name)
            Catch ex As Exception
            End Try

            If IsNothing(logWsheet) Then
                logWsheet = wb.Worksheets.Add()
                logWsheet.Name = masterConfObj.log_table_name
            End If
            logWsheet.Rows.Clear()

            If ctrl.logGlobalList.Count > 0 Then
                Dim arrayLog(ctrl.logGlobalList.Count, 6) As Object
                arrayLog(0, 0) = "ID"
                arrayLog(0, 1) = "Line"
                arrayLog(0, 2) = "Mat ID"
                arrayLog(0, 3) = "Code"
                arrayLog(0, 4) = "Column"
                arrayLog(0, 5) = "Type"
                arrayLog(0, 6) = "Description"
                Dim k = 1
                For Each logE As CLogEntry In ctrl.logGlobalList
                    arrayLog(k, 0) = k
                    arrayLog(k, 1) = logE.line
                    arrayLog(k, 2) = logE.mat_id
                    arrayLog(k, 3) = logE.code
                    arrayLog(k, 4) = logE.column
                    arrayLog(k, 5) = logE.log_type
                    arrayLog(k, 6) = logE.description
                    k = k + 1
                Next
                xlrg = CType(logWsheet.Cells(1, "A"), Microsoft.Office.Interop.Excel.Range)
                xlrg = logWsheet.Range(xlrg, xlrg.Offset(ctrl.logGlobalList.Count, 6))
                xlrg.Value = arrayLog
            End If
            'log
            ctrl.addDetailsLog("Save the template file")
            Try
                wb.Save()
                ctrl.addDetailsLog("Workbook successfully saved")
            Catch ex As Exception
                ctrl.addDetailsLog("Workbook failed to save")
            End Try
            MsgBox("Load Ended")
        Catch ex As Exception
            Throw New Exception("An error occured while filling the template array" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        Finally
            MUtils.releaseObject(wb_sht)
            MUtils.releaseObject(wb)
            MUtils.releaseObject(logWsheet)
            ctrl.xlFileHandler.OnCalc(ctrl.xlFileHandler.xlappUser)
        End Try
    End Sub

    Public Sub fillArray(tmplE As CTmplEntry, i As Integer, ByRef myArray(,) As String)
        For Each j As Integer In ctrl.confController.tmplDataConfListReverse.Keys
            myArray(i, j - 1) = CallByName(tmplE, ctrl.confController.tmplDataConfListReverse(j), CallType.Get)
        Next
    End Sub

    Public Function getBOWEntryVirtualCode(bow_entry As BOWEntry) As String
        Dim res As String = ""
        If Not String.IsNullOrWhiteSpace(bow_entry.virtual_code) Then
            res = bow_entry.virtual_code
        Else
            res = bow_entry_dict(bow_entry.code_htl).virtual_code
        End If
        If res.IndexOfAny("[~`!@#$%^&*()=|{}':;.,<>/?]+".ToCharArray) <> -1 Then
            Dim charList As New List(Of Char)
            charList = "[~`!@#$%^&*()=|{}':;.,<>/?]+".ToCharArray.ToList
            For Each mychar As Char In charList
                res = res.Replace(mychar, "_")
            Next
        End If
        Return res
    End Function

    Public Sub openConn(_conn_str As String)
        glb_oconn = New System.Data.OleDb.OleDbConnection(_conn_str)
        glb_oconn.Open()
    End Sub

End Class
