﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.panel = New System.Windows.Forms.Panel()
        Me.cb_ignore_mat = New System.Windows.Forms.CheckBox()
        Me.bt_transform = New System.Windows.Forms.Button()
        Me.bt_cancel = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.pbox_filepath = New System.Windows.Forms.PictureBox()
        Me.txt_bx_conf_file_path = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.open_file_dialog = New System.Windows.Forms.OpenFileDialog()
        Me.listbox_mainlog = New System.Windows.Forms.ListBox()
        Me.grp_logs = New System.Windows.Forms.GroupBox()
        Me.listbox_detailslog = New System.Windows.Forms.ListBox()
        Me.grp_exec_stat = New System.Windows.Forms.GroupBox()
        Me.bt_inf_link_cnt = New System.Windows.Forms.TextBox()
        Me.bt_ignored = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.bt_logs_cnt = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.bt_related_not_found = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.bt_htl_rel = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.bt_material = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.bt_etl = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.bt_ops = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.bt_htl = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.bt_mbx_doc = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.bt_success = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.bt_failed = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.bt_src_records = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.progress_bar = New System.Windows.Forms.ProgressBar()
        Me.MaskedTextBox1 = New System.Windows.Forms.MaskedTextBox()
        Me.sub_progress_bar = New System.Windows.Forms.ProgressBar()
        Me.sub_progress_bar_lbl = New System.Windows.Forms.Label()
        Me.cb_check_mat = New System.Windows.Forms.CheckBox()
        Me.panel.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.pbox_filepath, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grp_logs.SuspendLayout()
        Me.grp_exec_stat.SuspendLayout()
        Me.SuspendLayout()
        '
        'panel
        '
        Me.panel.Controls.Add(Me.cb_check_mat)
        Me.panel.Controls.Add(Me.cb_ignore_mat)
        Me.panel.Controls.Add(Me.bt_transform)
        Me.panel.Controls.Add(Me.bt_cancel)
        Me.panel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panel.Location = New System.Drawing.Point(0, 634)
        Me.panel.Name = "panel"
        Me.panel.Size = New System.Drawing.Size(1172, 65)
        Me.panel.TabIndex = 1
        '
        'cb_ignore_mat
        '
        Me.cb_ignore_mat.AutoSize = True
        Me.cb_ignore_mat.Location = New System.Drawing.Point(33, 20)
        Me.cb_ignore_mat.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cb_ignore_mat.Name = "cb_ignore_mat"
        Me.cb_ignore_mat.Size = New System.Drawing.Size(141, 24)
        Me.cb_ignore_mat.TabIndex = 4
        Me.cb_ignore_mat.Text = "Ignore Material"
        Me.cb_ignore_mat.UseVisualStyleBackColor = True
        '
        'bt_transform
        '
        Me.bt_transform.Location = New System.Drawing.Point(364, 15)
        Me.bt_transform.Name = "bt_transform"
        Me.bt_transform.Size = New System.Drawing.Size(104, 38)
        Me.bt_transform.TabIndex = 3
        Me.bt_transform.Text = "transform"
        Me.bt_transform.UseVisualStyleBackColor = True
        '
        'bt_cancel
        '
        Me.bt_cancel.Location = New System.Drawing.Point(820, 15)
        Me.bt_cancel.Name = "bt_cancel"
        Me.bt_cancel.Size = New System.Drawing.Size(104, 38)
        Me.bt_cancel.TabIndex = 0
        Me.bt_cancel.Text = "cancel"
        Me.bt_cancel.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.pbox_filepath)
        Me.GroupBox1.Controls.Add(Me.txt_bx_conf_file_path)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 45)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1148, 74)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "config source"
        '
        'pbox_filepath
        '
        Me.pbox_filepath.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pbox_filepath.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbox_filepath.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pbox_filepath.Image = Global.S4MigTaskListTmplFill.My.Resources.Resources.folder_open
        Me.pbox_filepath.Location = New System.Drawing.Point(1108, 26)
        Me.pbox_filepath.Name = "pbox_filepath"
        Me.pbox_filepath.Size = New System.Drawing.Size(33, 35)
        Me.pbox_filepath.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbox_filepath.TabIndex = 2
        Me.pbox_filepath.TabStop = False
        '
        'txt_bx_conf_file_path
        '
        Me.txt_bx_conf_file_path.Location = New System.Drawing.Point(146, 35)
        Me.txt_bx_conf_file_path.Name = "txt_bx_conf_file_path"
        Me.txt_bx_conf_file_path.Size = New System.Drawing.Size(956, 26)
        Me.txt_bx_conf_file_path.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(123, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Config file path :"
        '
        'open_file_dialog
        '
        Me.open_file_dialog.FileName = "OpenFileDialog1"
        '
        'listbox_mainlog
        '
        Me.listbox_mainlog.FormattingEnabled = True
        Me.listbox_mainlog.HorizontalScrollbar = True
        Me.listbox_mainlog.ItemHeight = 20
        Me.listbox_mainlog.Location = New System.Drawing.Point(6, 25)
        Me.listbox_mainlog.Name = "listbox_mainlog"
        Me.listbox_mainlog.Size = New System.Drawing.Size(380, 224)
        Me.listbox_mainlog.TabIndex = 3
        '
        'grp_logs
        '
        Me.grp_logs.Controls.Add(Me.listbox_detailslog)
        Me.grp_logs.Controls.Add(Me.listbox_mainlog)
        Me.grp_logs.Location = New System.Drawing.Point(12, 125)
        Me.grp_logs.Name = "grp_logs"
        Me.grp_logs.Size = New System.Drawing.Size(1148, 256)
        Me.grp_logs.TabIndex = 4
        Me.grp_logs.TabStop = False
        Me.grp_logs.Text = "Logs"
        '
        'listbox_detailslog
        '
        Me.listbox_detailslog.FormattingEnabled = True
        Me.listbox_detailslog.HorizontalScrollbar = True
        Me.listbox_detailslog.ItemHeight = 20
        Me.listbox_detailslog.Location = New System.Drawing.Point(392, 25)
        Me.listbox_detailslog.Name = "listbox_detailslog"
        Me.listbox_detailslog.Size = New System.Drawing.Size(750, 224)
        Me.listbox_detailslog.TabIndex = 4
        '
        'grp_exec_stat
        '
        Me.grp_exec_stat.Controls.Add(Me.bt_inf_link_cnt)
        Me.grp_exec_stat.Controls.Add(Me.bt_ignored)
        Me.grp_exec_stat.Controls.Add(Me.Label14)
        Me.grp_exec_stat.Controls.Add(Me.Label13)
        Me.grp_exec_stat.Controls.Add(Me.bt_logs_cnt)
        Me.grp_exec_stat.Controls.Add(Me.Label12)
        Me.grp_exec_stat.Controls.Add(Me.bt_related_not_found)
        Me.grp_exec_stat.Controls.Add(Me.Label11)
        Me.grp_exec_stat.Controls.Add(Me.bt_htl_rel)
        Me.grp_exec_stat.Controls.Add(Me.Label10)
        Me.grp_exec_stat.Controls.Add(Me.bt_material)
        Me.grp_exec_stat.Controls.Add(Me.Label9)
        Me.grp_exec_stat.Controls.Add(Me.bt_etl)
        Me.grp_exec_stat.Controls.Add(Me.Label8)
        Me.grp_exec_stat.Controls.Add(Me.bt_ops)
        Me.grp_exec_stat.Controls.Add(Me.Label7)
        Me.grp_exec_stat.Controls.Add(Me.bt_htl)
        Me.grp_exec_stat.Controls.Add(Me.Label6)
        Me.grp_exec_stat.Controls.Add(Me.bt_mbx_doc)
        Me.grp_exec_stat.Controls.Add(Me.Label5)
        Me.grp_exec_stat.Controls.Add(Me.bt_success)
        Me.grp_exec_stat.Controls.Add(Me.Label4)
        Me.grp_exec_stat.Controls.Add(Me.bt_failed)
        Me.grp_exec_stat.Controls.Add(Me.Label3)
        Me.grp_exec_stat.Controls.Add(Me.bt_src_records)
        Me.grp_exec_stat.Controls.Add(Me.Label2)
        Me.grp_exec_stat.Location = New System.Drawing.Point(12, 387)
        Me.grp_exec_stat.Name = "grp_exec_stat"
        Me.grp_exec_stat.Size = New System.Drawing.Size(1148, 142)
        Me.grp_exec_stat.TabIndex = 5
        Me.grp_exec_stat.TabStop = False
        Me.grp_exec_stat.Text = "Execution Stats"
        '
        'bt_inf_link_cnt
        '
        Me.bt_inf_link_cnt.Enabled = False
        Me.bt_inf_link_cnt.Location = New System.Drawing.Point(686, 54)
        Me.bt_inf_link_cnt.Name = "bt_inf_link_cnt"
        Me.bt_inf_link_cnt.Size = New System.Drawing.Size(62, 26)
        Me.bt_inf_link_cnt.TabIndex = 20
        '
        'bt_ignored
        '
        Me.bt_ignored.Enabled = False
        Me.bt_ignored.Location = New System.Drawing.Point(249, 85)
        Me.bt_ignored.Name = "bt_ignored"
        Me.bt_ignored.Size = New System.Drawing.Size(78, 26)
        Me.bt_ignored.TabIndex = 24
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(608, 57)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(73, 20)
        Me.Label14.TabIndex = 19
        Me.Label14.Text = "Inf. Link :"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(86, 88)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(72, 20)
        Me.Label13.TabIndex = 23
        Me.Label13.Text = "Ignored :"
        '
        'bt_logs_cnt
        '
        Me.bt_logs_cnt.Enabled = False
        Me.bt_logs_cnt.Location = New System.Drawing.Point(804, 54)
        Me.bt_logs_cnt.Name = "bt_logs_cnt"
        Me.bt_logs_cnt.Size = New System.Drawing.Size(62, 26)
        Me.bt_logs_cnt.TabIndex = 22
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(747, 57)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(52, 20)
        Me.Label12.TabIndex = 21
        Me.Label12.Text = "Logs :"
        '
        'bt_related_not_found
        '
        Me.bt_related_not_found.Enabled = False
        Me.bt_related_not_found.Location = New System.Drawing.Point(540, 51)
        Me.bt_related_not_found.Name = "bt_related_not_found"
        Me.bt_related_not_found.Size = New System.Drawing.Size(62, 26)
        Me.bt_related_not_found.TabIndex = 20
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(369, 57)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(148, 20)
        Me.Label11.TabIndex = 19
        Me.Label11.Text = "Related Not Found:"
        '
        'bt_htl_rel
        '
        Me.bt_htl_rel.Enabled = False
        Me.bt_htl_rel.Location = New System.Drawing.Point(686, 18)
        Me.bt_htl_rel.Name = "bt_htl_rel"
        Me.bt_htl_rel.Size = New System.Drawing.Size(62, 26)
        Me.bt_htl_rel.TabIndex = 18
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(608, 22)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(79, 20)
        Me.Label10.TabIndex = 17
        Me.Label10.Text = "Rel. HTL :"
        '
        'bt_material
        '
        Me.bt_material.Enabled = False
        Me.bt_material.Location = New System.Drawing.Point(1065, 18)
        Me.bt_material.Name = "bt_material"
        Me.bt_material.Size = New System.Drawing.Size(62, 26)
        Me.bt_material.TabIndex = 16
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(1010, 22)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(48, 20)
        Me.Label9.TabIndex = 15
        Me.Label9.Text = "Mat. :"
        '
        'bt_etl
        '
        Me.bt_etl.Enabled = False
        Me.bt_etl.Location = New System.Drawing.Point(806, 18)
        Me.bt_etl.Name = "bt_etl"
        Me.bt_etl.Size = New System.Drawing.Size(62, 26)
        Me.bt_etl.TabIndex = 14
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(753, 22)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(46, 20)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "ETL :"
        '
        'bt_ops
        '
        Me.bt_ops.Enabled = False
        Me.bt_ops.Location = New System.Drawing.Point(928, 18)
        Me.bt_ops.Name = "bt_ops"
        Me.bt_ops.Size = New System.Drawing.Size(62, 26)
        Me.bt_ops.TabIndex = 12
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(873, 22)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(50, 20)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Ops. :"
        '
        'bt_htl
        '
        Me.bt_htl.Enabled = False
        Me.bt_htl.Location = New System.Drawing.Point(540, 18)
        Me.bt_htl.Name = "bt_htl"
        Me.bt_htl.Size = New System.Drawing.Size(62, 26)
        Me.bt_htl.TabIndex = 10
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(490, 22)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 20)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "HTL :"
        '
        'bt_mbx_doc
        '
        Me.bt_mbx_doc.Enabled = False
        Me.bt_mbx_doc.Location = New System.Drawing.Point(422, 18)
        Me.bt_mbx_doc.Name = "bt_mbx_doc"
        Me.bt_mbx_doc.Size = New System.Drawing.Size(62, 26)
        Me.bt_mbx_doc.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(332, 22)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(89, 20)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "MBX Doc. :"
        '
        'bt_success
        '
        Me.bt_success.Enabled = False
        Me.bt_success.Location = New System.Drawing.Point(249, 18)
        Me.bt_success.Name = "bt_success"
        Me.bt_success.Size = New System.Drawing.Size(78, 26)
        Me.bt_success.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(160, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(93, 20)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "successed :"
        '
        'bt_failed
        '
        Me.bt_failed.Enabled = False
        Me.bt_failed.Location = New System.Drawing.Point(249, 51)
        Me.bt_failed.Name = "bt_failed"
        Me.bt_failed.Size = New System.Drawing.Size(78, 26)
        Me.bt_failed.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(160, 57)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 20)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "failed :"
        '
        'bt_src_records
        '
        Me.bt_src_records.Enabled = False
        Me.bt_src_records.Location = New System.Drawing.Point(76, 18)
        Me.bt_src_records.Name = "bt_src_records"
        Me.bt_src_records.Size = New System.Drawing.Size(78, 26)
        Me.bt_src_records.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 20)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "source :"
        '
        'progress_bar
        '
        Me.progress_bar.Location = New System.Drawing.Point(12, 593)
        Me.progress_bar.Name = "progress_bar"
        Me.progress_bar.Size = New System.Drawing.Size(1148, 26)
        Me.progress_bar.TabIndex = 6
        '
        'MaskedTextBox1
        '
        Me.MaskedTextBox1.Enabled = False
        Me.MaskedTextBox1.Location = New System.Drawing.Point(12, 2)
        Me.MaskedTextBox1.Name = "MaskedTextBox1"
        Me.MaskedTextBox1.Size = New System.Drawing.Size(180, 26)
        Me.MaskedTextBox1.TabIndex = 8
        Me.MaskedTextBox1.Text = "Version 1.0 Feb 22"
        '
        'sub_progress_bar
        '
        Me.sub_progress_bar.Location = New System.Drawing.Point(12, 535)
        Me.sub_progress_bar.Name = "sub_progress_bar"
        Me.sub_progress_bar.Size = New System.Drawing.Size(1148, 26)
        Me.sub_progress_bar.TabIndex = 9
        '
        'sub_progress_bar_lbl
        '
        Me.sub_progress_bar_lbl.AutoSize = True
        Me.sub_progress_bar_lbl.Location = New System.Drawing.Point(8, 570)
        Me.sub_progress_bar_lbl.Name = "sub_progress_bar_lbl"
        Me.sub_progress_bar_lbl.Size = New System.Drawing.Size(78, 20)
        Me.sub_progress_bar_lbl.TabIndex = 25
        Me.sub_progress_bar_lbl.Text = "Loading..."
        '
        'cb_check_mat
        '
        Me.cb_check_mat.AutoSize = True
        Me.cb_check_mat.Checked = True
        Me.cb_check_mat.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cb_check_mat.Location = New System.Drawing.Point(176, 20)
        Me.cb_check_mat.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cb_check_mat.Name = "cb_check_mat"
        Me.cb_check_mat.Size = New System.Drawing.Size(153, 24)
        Me.cb_check_mat.TabIndex = 5
        Me.cb_check_mat.Text = "Check Material ?"
        Me.cb_check_mat.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1172, 699)
        Me.Controls.Add(Me.sub_progress_bar_lbl)
        Me.Controls.Add(Me.sub_progress_bar)
        Me.Controls.Add(Me.MaskedTextBox1)
        Me.Controls.Add(Me.progress_bar)
        Me.Controls.Add(Me.grp_exec_stat)
        Me.Controls.Add(Me.grp_logs)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.panel)
        Me.Name = "Form1"
        Me.Text = "Fill Task List Template"
        Me.panel.ResumeLayout(False)
        Me.panel.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.pbox_filepath, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grp_logs.ResumeLayout(False)
        Me.grp_exec_stat.ResumeLayout(False)
        Me.grp_exec_stat.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents panel As Panel
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txt_bx_conf_file_path As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents pbox_filepath As PictureBox
    Friend WithEvents open_file_dialog As OpenFileDialog
    Friend WithEvents bt_cancel As Button
    Friend WithEvents bt_transform As Button
    Friend WithEvents listbox_mainlog As ListBox
    Friend WithEvents grp_logs As GroupBox
    Friend WithEvents listbox_detailslog As ListBox
    Friend WithEvents grp_exec_stat As GroupBox
    Friend WithEvents bt_success As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents bt_failed As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents bt_src_records As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents bt_ops As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents bt_htl As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents bt_mbx_doc As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents bt_etl As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents bt_material As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents bt_htl_rel As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents progress_bar As ProgressBar
    Friend WithEvents bt_related_not_found As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents bt_logs_cnt As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents bt_ignored As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents MaskedTextBox1 As MaskedTextBox
    Friend WithEvents bt_inf_link_cnt As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents cb_ignore_mat As CheckBox
    Friend WithEvents sub_progress_bar As ProgressBar
    Friend WithEvents sub_progress_bar_lbl As Label
    Friend WithEvents cb_check_mat As CheckBox
End Class
