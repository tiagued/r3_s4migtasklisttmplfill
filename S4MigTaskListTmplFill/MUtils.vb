﻿Module MUtils
    Public Sub releaseObject(ByVal obj As Object)
        Try
            If IsNothing(obj) Then
                Exit Sub
            End If
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        End Try
    End Sub
End Module
