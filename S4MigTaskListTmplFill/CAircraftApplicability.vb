﻿Public Class CAircraftApplicability
    Private _id As String
    Public Property id() As String
        Get
            Return _id
        End Get
        Set(ByVal value As String)
            _id = value
        End Set
    End Property

    Private _ac_manuf As String
    Public Property ac_manuf() As String
        Get
            Return _ac_manuf
        End Get
        Set(ByVal value As String)
            _ac_manuf = value
        End Set
    End Property

    Private _ac_type As String
    Public Property ac_type() As String
        Get
            Return _ac_type
        End Get
        Set(ByVal value As String)
            _ac_type = value
        End Set
    End Property

    Private _ac_model As String
    Public Property ac_model() As String
        Get
            Return _ac_model
        End Get
        Set(ByVal value As String)
            _ac_model = value
        End Set
    End Property

End Class
