﻿Public Class CLogEntry
    Public Const LOG_TYPE_ERROR As String = "ERROR"
    Public Const LOG_TYPE_WARNING As String = "WARNING"
    Public Const LOG_TYPE_INFO As String = "INFO"

    Private _log_type As String
    Public Property log_type() As String
        Get
            Return _log_type
        End Get
        Set(ByVal value As String)
            _log_type = value
        End Set
    End Property

    Private _line As String
    Public Property line() As String
        Get
            Return _line
        End Get
        Set(ByVal value As String)
            _line = value
        End Set
    End Property

    Private _column As String
    Public Property column() As String
        Get
            Return _column
        End Get
        Set(ByVal value As String)
            _column = value
        End Set
    End Property

    Private _mat_id As String
    Public Property mat_id() As String
        Get
            Return _mat_id
        End Get
        Set(ByVal value As String)
            _mat_id = value
        End Set
    End Property

    Private _code As String
    Public Property code() As String
        Get
            Return _code
        End Get
        Set(ByVal value As String)
            _code = value
        End Set
    End Property


    Private _description As String
    Public Property description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

End Class
