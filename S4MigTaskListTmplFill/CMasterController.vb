﻿Imports System.Threading

Public Class CMasterController
    'reference to the main form
    Public mainForm As Form1
    Public xlFileHandler As CExcelInteropHandler
    Public confController As CConfigController
    Public file_path As String
    Public transform_ctrl As CReadTransformController
    Public source_cnt As Integer
    Public logs_cnt As Integer
    Public success_cnt As Integer
    Public ignored_cnt As Integer
    Public failed_cnt As Integer
    Public duplicated_serv_cnt As Integer
    Public mbx_doc_cnt As Integer
    Public htl_cnt As Integer
    Public htl_rel_cnt As Integer
    Public etl_cnt As Integer
    Public operation_cnt As Integer
    Public related_not_found_cnt As Integer
    Public material_cnt As Integer
    Public material_ignored_cnt As Integer
    Public related_serv_ignored_cnt As Integer
    Public serv_task_ignored_cnt As Integer
    Public infinite_link_cnt As Integer
    Public child_related_of_child_cnt As Integer
    Public infinite_circular_link_cnt As Integer
    Public check_mat As Boolean
    'conn string to cs file
    Public cs_conn As String
    Public logGlobalList As List(Of CLogEntry)
    Public ignore_material As Boolean

    Public Sub New(form As Form1)
        Try
            transform_ctrl = New CReadTransformController(Me)
            confController = New CConfigController(Me)
            mainForm = form
            xlFileHandler = New CExcelInteropHandler
            logGlobalList = New List(Of CLogEntry)
            mainForm.txt_bx_conf_file_path.Text = "\\netappbsl21\JBSLGroups\GLOBAL-Quotation\05_Quotation_Support\98_Projects\SAP MDG\Migration upload files\task lists\R3_To_S4\TL_CONF_R3S4.xlsx"
        Catch ex As Exception
            Throw New Exception("Loading Application" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
    End Sub

    'read data from source, transform and fill migration template
    Public Sub transform()
        Try
            check_mat = mainForm.cb_check_mat.Checked
            mainForm.listbox_mainlog.Items.Clear()
            mainForm.listbox_detailslog.Items.Clear()

            mainForm.listbox_detailslog.Items.Clear()
            mainForm.listbox_mainlog.Items.Clear()
            setStatistics()
            addMainLog("Strat transforming...")
            setProgressBar(0)
            logGlobalList.Clear()
            ignore_material = mainForm.cb_ignore_mat.Checked

            'set ctrl
            MMigrationRules.ctrl = Me
            'read config
            addMainLog("Load configuration")
            confController.loadConfing(mainForm.txt_bx_conf_file_path.Text)
            incrementProgressBar(20)
            Application.DoEvents()
            'load & transform
            addMainLog("Read GQT file and transform data")
            Application.DoEvents()
            transform_ctrl.loadAndTransformSourceData()
            setProgressBar(100)
            failed_cnt = source_cnt - success_cnt - ignored_cnt
            setStatistics()
            addMainLog("....Transforming End")
        Catch ex As Exception
            MsgBox("An error occured while Transforming Data" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub


    Public Sub getConfFile()
        'open file dialog
        Dim dialogRes As DialogResult
        Try
            mainForm.open_file_dialog.AddExtension = True
            mainForm.open_file_dialog.CheckFileExists = True
            mainForm.open_file_dialog.CheckPathExists = True
            mainForm.open_file_dialog.InitialDirectory = "\\netappbsl21\JBSLGroups\GLOBAL-Quotation\05_Quotation_Support\98_Projects\SAP MDG\Migration upload files\task lists" 
            mainForm.open_file_dialog.Multiselect = False
            mainForm.open_file_dialog.DereferenceLinks = True
            'filter file types
            mainForm.open_file_dialog.Filter = "Load Tool config file|*xlsx"

            dialogRes = mainForm.open_file_dialog.ShowDialog()

            If dialogRes = DialogResult.OK Then
                'check file extention
                mainForm.txt_bx_conf_file_path.Text = mainForm.open_file_dialog.FileName
            End If
        Catch ex As Exception
            MsgBox("Picking File Dialog loading" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub addMainLog(text As String)
        mainForm.listbox_mainlog.Items.Add(DateTime.Now.ToString("hh:mm:ss") & " " & text)
        mainForm.listbox_mainlog.ClearSelected()
        mainForm.listbox_mainlog.SelectedIndex = mainForm.listbox_mainlog.Items.Count - 1
        addDetailsLog(text)
    End Sub
    Public Sub addDetailsLog(text As String)
        mainForm.listbox_detailslog.Items.Add(DateTime.Now.ToString("hh:mm:ss") & " " & text)
        mainForm.listbox_detailslog.ClearSelected()
        mainForm.listbox_detailslog.SelectedIndex = mainForm.listbox_detailslog.Items.Count - 1
    End Sub

    Public Sub setStatistics()
        mainForm.bt_src_records.Text = source_cnt
        mainForm.bt_success.Text = success_cnt
        mainForm.bt_failed.Text = failed_cnt
        mainForm.bt_ignored.Text = ignored_cnt
        mainForm.bt_mbx_doc.Text = mbx_doc_cnt
        mainForm.bt_htl.Text = htl_cnt
        mainForm.bt_htl_rel.Text = htl_rel_cnt
        mainForm.bt_etl.Text = etl_cnt
        mainForm.bt_ops.Text = operation_cnt
        mainForm.bt_material.Text = material_cnt
        mainForm.bt_inf_link_cnt.Text = infinite_link_cnt
        'material_ignored_cnt
        mainForm.bt_related_not_found.Text = related_not_found_cnt
        mainForm.bt_logs_cnt.Text = logs_cnt
    End Sub

    Public Sub setProgressBar(percent As Integer)
        If percent > 100 Then
            mainForm.progress_bar.Value = 100
        ElseIf percent < 0 Then
            mainForm.progress_bar.Value = percent
        Else
            mainForm.progress_bar.Value = percent
        End If
        Application.DoEvents()
    End Sub

    Public Sub incrementProgressBar(percent As Integer)
        mainForm.progress_bar.Value = mainForm.progress_bar.Value + percent
        Application.DoEvents()
    End Sub

    Public Sub setSubProgressBar(percent As Integer)
        If percent > 100 Then
            mainForm.sub_progress_bar.Value = 100
        ElseIf percent < 0 Then
            mainForm.sub_progress_bar.Value = percent
        Else
            mainForm.sub_progress_bar.Value = percent
        End If
        Application.DoEvents()
    End Sub

    Public Sub incrementSubProgressBar(percent As Integer)
        mainForm.sub_progress_bar.Value = mainForm.sub_progress_bar.Value + percent
        Application.DoEvents()
    End Sub

End Class
