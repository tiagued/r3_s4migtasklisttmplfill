﻿Module MMigrationRules
    Public ctrl As CMasterController

    'MBX MType calculation from GQT mat_type
    Public Function getTmplMType(gqtEntry As BOWEntry) As KeyValuePair(Of String, CLogEntry)
        Dim res As String = ""
        Dim log As CLogEntry = Nothing
        If Not String.IsNullOrWhiteSpace(gqtEntry.notif_type) AndAlso ctrl.confController.mtypeCorrespondanceList.ContainsKey(UCase(gqtEntry.notif_type)) Then
            res = ctrl.confController.mtypeCorrespondanceList(UCase(gqtEntry.notif_type))
        Else
            log = New CLogEntry
            log.line = gqtEntry.line
            log.mat_id = gqtEntry.htl_id
            log.code = gqtEntry.code
            log.column = "mat_type" & ", col " & ctrl.confController.srcDataConfList("notif_type")
            log.log_type = CLogEntry.LOG_TYPE_ERROR
            log.description = "GQT Mat_type value " & gqtEntry.notif_type & " has not been found in the correspondance table"
        End If
        Return New KeyValuePair(Of String, CLogEntry)(res, log)
    End Function
    'Work center type
    Public Function getWorkCenter(bow_entry As BOWEntry, mainAppl As CAircraftApplicability) As KeyValuePair(Of String, CLogEntry)
        Dim res As String = ""
        Dim log As CLogEntry = Nothing
        If Not String.IsNullOrWhiteSpace(bow_entry.work_center_txt) AndAlso ctrl.confController.workCenterCorrespondanceList.ContainsKey(UCase(bow_entry.work_center_txt)) Then
            res = ctrl.confController.workCenterCorrespondanceList(UCase(bow_entry.work_center_txt))
        ElseIf Not String.IsNullOrWhiteSpace(bow_entry.work_center_txt) AndAlso ctrl.confController.workCenterCorrespondanceList.ContainsKey(UCase(bow_entry.work_center_txt & "_" & mainAppl.ac_manuf)) Then
            res = ctrl.confController.workCenterCorrespondanceList(UCase(bow_entry.work_center_txt & "_" & mainAppl.ac_manuf))
        Else
            log = New CLogEntry
            log.line = bow_entry.line
            log.mat_id = bow_entry.htl_id
            log.code = bow_entry.code
            log.column = "cost_center"
            log.log_type = CLogEntry.LOG_TYPE_ERROR
            log.description = "BOW cost center " & bow_entry.work_center_txt & " has not been found in the correspondance table"
        End If
        Return New KeyValuePair(Of String, CLogEntry)(res, log)
    End Function

    'PMPS Reference
    Public Function getPMPSReference(bow_entry As BOWEntry) As KeyValuePair(Of String, CLogEntry)
        Dim res As String = ""
        Dim log As CLogEntry = Nothing
        If Not String.IsNullOrWhiteSpace(bow_entry.pm_ps) AndAlso ctrl.confController.PMPSCorrespondanceList.ContainsKey(UCase(bow_entry.pm_ps)) Then
            res = ctrl.confController.PMPSCorrespondanceList(UCase(bow_entry.pm_ps))
        Else
            log = New CLogEntry
            log.line = bow_entry.line
            log.mat_id = bow_entry.htl_id
            log.code = bow_entry.code
            log.column = "PM PS Ref"
            log.log_type = CLogEntry.LOG_TYPE_ERROR
            log.description = "PM PS Reference " & bow_entry.pm_ps & " has not been found in the correspondance table"
        End If
        Return New KeyValuePair(Of String, CLogEntry)(res, log)
    End Function

    'RO-INCOMING
    'RO-REMOVE
    'RO-ROUTINE INSPECTION
    'RO-ROUTINE MAINTENANCE
    'DR-DEFECTS
    'RO-INSTALL - FIT
    'RO-FUNCTIONAL CHECKS - HANGAR
    'RO-FUNCTIONAL CHECKS - RAMP
    'RO-FLIGHT CHECK
    'RO-PRE DEPARTURE
    'RO-REDELIVERY
    'PT-REMOVE
    'PT-PAINT - PREPARATION
    'PT-PAINT - STRIP And SAND
    'PT-PAINT - PRECOAT
    'PT-PAINT - PAINT
    'PT-FIT
    'RMUC-DESIGN
    'RMUC-PROCURE, FAB And KIT
    'RMUC-MODIFY
    'RMUC-REFURBISH
    'RMUC-CERTIFY
    Public Sub setPanelPMPSReference(etl As CTmplEntry)
        If etl.is_panel Then
            etl.pm_ps = "RO-PANEL-REMOVE"
            etl.etl_entry_close_panel.pm_ps = "RO-PANEL-INSTALL-FIT"
        End If
    End Sub

    'description rules for MBX Doc
    Public Sub setMBXDocDescriptionRules(mbxDoc As CTmplEntry)
        'desc=40 char, remaining part in long_descr up to 200
        If Not String.IsNullOrWhiteSpace(mbxDoc.description) AndAlso mbxDoc.description.Length > 20 Then
            Dim full_desc As String = mbxDoc.description
            If full_desc.Length <= 40 Then
                mbxDoc.description = full_desc
            Else
                mbxDoc.description = full_desc.Substring(0, 40)
                mbxDoc.long_text = full_desc.Substring(40, full_desc.Length - 40)
            End If
        End If
    End Sub

    'description rules for Operation
    Public Sub setOperationDescriptionRules(operation As CTmplEntry)
        'desc=40 char, remaining part in long_descr up to 200
        If Not String.IsNullOrWhiteSpace(operation.op_text) AndAlso operation.op_text.Length > 20 Then
            Dim full_desc As String = operation.op_text
            If full_desc.Length <= 40 Then
                operation.op_text = UCase(full_desc)
            Else
                operation.op_text = UCase(full_desc.Substring(0, 40))
                operation.op_long_text = UCase(full_desc.Substring(40, full_desc.Length - 40))
            End If
        End If
    End Sub

    'Set ATA Code
    Public Sub setMBXDocATACode(mbxDoc As CTmplEntry)
        'desc=40 char, remaining part in long_descr up to 200
        If ctrl.confController.defaultValueList("ata_code") = "operator_code_two_first_digit" Then
            Dim externalCodeInt As Integer
            If Integer.TryParse(Trim(mbxDoc.external_ref_no), externalCodeInt) Then
                If mbxDoc.external_ref_no.Length > 1 Then
                    mbxDoc.ata = mbxDoc.external_ref_no.Substring(0, 2)
                Else
                    mbxDoc.ata = "0" & mbxDoc.external_ref_no.Substring(0, 1)
                End If
            Else
                mbxDoc.ata = "12"
            End If
        End If
    End Sub

    'add statistics to logs
    Public Sub addStatisticsToLogs()

        Dim Log As CLogEntry

        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: Logs"
        Log.code = "Stats: Logs"
        Log.column = "Stats: Logs"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.logs_cnt
        ctrl.logGlobalList.Insert(0, Log)

        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: Duplicated Services"
        Log.code = "Stats: Duplicated Services"
        Log.column = "Stats: Duplicated Services"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.duplicated_serv_cnt
        ctrl.logGlobalList.Insert(0, Log)

        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: Ignored Service Tasks"
        Log.code = "Stats: Ignored Service Tasks"
        Log.column = "Stats: Ignored Service Tasks"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.serv_task_ignored_cnt
        ctrl.logGlobalList.Insert(0, Log)

        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: Ignored Related Service"
        Log.code = "Stats: Ignored Related Service"
        Log.column = "Stats: Ignored Related Service"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.related_serv_ignored_cnt
        ctrl.logGlobalList.Insert(0, Log)

        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: Ignored Materials"
        Log.code = "Stats: Ignored Materials"
        Log.column = "Stats: Ignored Materials"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.material_ignored_cnt
        ctrl.logGlobalList.Insert(0, Log)

        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: Related Not Found"
        Log.code = "Stats: Related Not Found"
        Log.column = "Stats: Related Not Found"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.related_not_found_cnt
        ctrl.logGlobalList.Insert(0, Log)

        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: Infinite Link"
        Log.code = "Stats: Infinite Link"
        Log.column = "Stats: Infinite Link"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.infinite_link_cnt
        ctrl.logGlobalList.Insert(0, Log)

        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: Circular Infinite Link"
        Log.code = "Stats: Circular Infinite Link"
        Log.column = "Stats: Circular Infinite Link"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.infinite_circular_link_cnt
        ctrl.logGlobalList.Insert(0, Log)

        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: ignored child linked to another child"
        Log.code = "Stats: ignored child linked to another child"
        Log.column = "Stats: ignored child linked to another child"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.child_related_of_child_cnt
        ctrl.logGlobalList.Insert(0, Log)

        'Mat
        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: Mat Created"
        Log.code = "Stats: Mat Created"
        Log.column = "Stats: Mat Created"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.material_cnt
        ctrl.logGlobalList.Insert(0, Log)

        'OPS
        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: Ops Created"
        Log.code = "Stats: Ops Created"
        Log.column = "Stats: Ops Created"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.operation_cnt
        ctrl.logGlobalList.Insert(0, Log)

        'ETL
        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: ETL Created"
        Log.code = "Stats: ETL Created"
        Log.column = "Stats: ETL Created"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.etl_cnt
        ctrl.logGlobalList.Insert(0, Log)

        'HTL
        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: Related HTL Created"
        Log.code = "Stats: Related HTL Created"
        Log.column = "Stats: Related HTL Created"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.htl_rel_cnt
        ctrl.logGlobalList.Insert(0, Log)

        'HTL
        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: HTL Created"
        Log.code = "Stats: HTL Created"
        Log.column = "Stats: HTL Created"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.htl_cnt
        ctrl.logGlobalList.Insert(0, Log)

        'MBX Doc
        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: MBX Doc Created"
        Log.code = "Stats: MBX Doc Created"
        Log.column = "Stats: MBX Doc Created"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.mbx_doc_cnt
        ctrl.logGlobalList.Insert(0, Log)

        'could not load
        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: Source Failed"
        Log.code = "Stats: Source Failed"
        Log.column = "Stats: Source Failed"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.failed_cnt
        ctrl.logGlobalList.Insert(0, Log)

        'load with success
        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: Source Succeeded"
        Log.code = "Stats: Source Succeeded"
        Log.column = "Stats: Source Succeeded"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.success_cnt
        ctrl.logGlobalList.Insert(0, Log)

        'ignored by load
        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: Source Ignored"
        Log.code = "Stats: Source Ignored"
        Log.column = "Stats: Source Ignored"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.ignored_cnt
        ctrl.logGlobalList.Insert(0, Log)

        'gqt entries
        Log = New CLogEntry
        Log.line = 0
        Log.mat_id = "Stats: GQT Entries"
        Log.code = "Stats: GQT Entries"
        Log.column = "Stats: GQT Entries"
        Log.log_type = CLogEntry.LOG_TYPE_INFO
        Log.description = ctrl.source_cnt
        ctrl.logGlobalList.Insert(0, Log)
    End Sub
End Module