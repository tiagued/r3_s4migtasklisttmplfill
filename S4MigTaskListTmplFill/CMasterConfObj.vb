﻿Public Class CMasterConfObj

    Public appl_list As List(Of CAircraftApplicability)
    Public Sub New()
        appl_list = New List(Of CAircraftApplicability)
    End Sub

    Private _id As String
    Public Property id() As String
        Get
            Return _id
        End Get
        Set(ByVal value As String)
            _id = value
        End Set
    End Property

    Private _bow_file_path As String
    Public Property bow_file_path() As String
        Get
            Return _bow_file_path
        End Get
        Set(ByVal value As String)
            _bow_file_path = value
        End Set
    End Property

    Private _bow_tab As String
    Public Property bow_tab() As String
        Get
            Return _bow_tab
        End Get
        Set(ByVal value As String)
            _bow_tab = value
        End Set
    End Property

    Private _task_list_tmpl_file As String
    Public Property task_list_tmpl_file() As String
        Get
            Return _task_list_tmpl_file
        End Get
        Set(ByVal value As String)
            _task_list_tmpl_file = value
        End Set
    End Property

    Private _task_list_tmpl_tab As String
    Public Property task_list_tmpl_tab() As String
        Get
            Return _task_list_tmpl_tab
        End Get
        Set(ByVal value As String)
            _task_list_tmpl_tab = value
        End Set
    End Property

    Private _task_list_tmpl_line_start As String
    Public Property task_list_tmpl_line_start() As String
        Get
            Return _task_list_tmpl_line_start
        End Get
        Set(ByVal value As String)
            _task_list_tmpl_line_start = value
        End Set
    End Property

    Private _task_list_tmpl_col_start As String
    Public Property task_list_tmpl_col_start() As String
        Get
            Return _task_list_tmpl_col_start
        End Get
        Set(ByVal value As String)
            _task_list_tmpl_col_start = value
        End Set
    End Property

    Private _log_table_name As String
    Public Property log_table_name() As String
        Get
            Return _log_table_name
        End Get
        Set(ByVal value As String)
            _log_table_name = value
        End Set
    End Property

    Private _src_id_ac_type_text As String
    Public Property src_id_ac_type_text() As String
        Get
            Return _src_id_ac_type_text
        End Get
        Set(ByVal value As String)
            _src_id_ac_type_text = value
        End Set
    End Property

    Private _engineering_dept As String
    Public Property engineering_dept() As String
        Get
            Return _engineering_dept
        End Get
        Set(ByVal value As String)
            _engineering_dept = value
        End Set
    End Property

    Private _issue_dt As String
    Public Property issue_dt() As String
        Get
            Return _issue_dt
        End Get
        Set(ByVal value As String)
            _issue_dt = value
        End Set
    End Property

    Private _upload As Boolean
    Public Property upload() As Boolean
        Get
            Return _upload
        End Get
        Set(ByVal value As Boolean)
            _upload = value
        End Set
    End Property


End Class
