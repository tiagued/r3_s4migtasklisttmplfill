﻿Public Class Form1
    Public controller As CMasterController
    Dim load_failed As Boolean
    Public Sub New()
        Try
            load_failed = False
            ' This call is required by the designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call.

            'instantiate controller
            controller = New CMasterController(Me)

        Catch ex As Exception
            MsgBox("Application Loading Error" & Chr(10) & ex.Message & ex.StackTrace, MsgBoxStyle.Critical)
            load_failed = True
        End Try
    End Sub
    Private Sub pbox_filepath_Click(sender As Object, e As EventArgs) Handles pbox_filepath.Click
        controller.getConfFile()
    End Sub

    Private Sub bt_cancel_Click(sender As Object, e As EventArgs) Handles bt_cancel.Click
        Me.Close()
    End Sub

    Private Sub bt_transform_Click(sender As Object, e As EventArgs) Handles bt_transform.Click
        Try
            controller.transform()
        Catch ex As Exception
            MsgBox("An error occured " & Chr(10) & ex.Message & ex.Message & ex.StackTrace)
        End Try
    End Sub

End Class
