﻿Module MMaterialList
    Public Sub loadMaterialList(ctrl As CReadTransformController)
        ctrl.migrated_material_list.Clear()
        'add mats
        Using conn = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=\\netappbsl21\JBSLGroups\GLOBAL-Quotation\05_Quotation_Support\98_Projects\SAP MDG\Migration upload files\task lists\Material_List.xlsx;Extended Properties=""Excel 12.0 Xml;HDR=YES""")
            conn.Open()
            Dim query = "Select * FROM [SAP_PN$];"
            Using cmd As New System.Data.OleDb.OleDbCommand(query, conn)
                Dim dt As New DataTable
                dt.Load(cmd.ExecuteReader)
                ctrl.migrated_material_list = (From item In dt.AsEnumerable() Select Trim(item.Field(Of String)(0))).ToList()
            End Using
        End Using
    End Sub
End Module
