﻿Public Class BOWEntry

    Public etl_list As Dictionary(Of String, BOWEntry)
    Public panel_list As Dictionary(Of String, BOWEntry)
    Public operation_list As Dictionary(Of String, BOWEntry)
    Public material_list As Dictionary(Of String, BOWEntry)

    Public Sub New()
        etl_list = New Dictionary(Of String, BOWEntry)
        panel_list = New Dictionary(Of String, BOWEntry)
        operation_list = New Dictionary(Of String, BOWEntry)
        material_list = New Dictionary(Of String, BOWEntry)
        _panel = ""
        _etl_id = ""
        _htl_id = ""
    End Sub

    Private _line As Integer
    Public Property line() As Integer
        Get
            Return _line
        End Get
        Set(ByVal value As Integer)
            _line = value
        End Set
    End Property


    Private _code As String
    Public Property code() As String
        Get
            Return _code
        End Get
        Set(ByVal value As String)
            _code = Trim(value)
        End Set
    End Property

    Private _virtual_code As String
    Public Property virtual_code() As String
        Get
            Return _virtual_code
        End Get
        Set(ByVal value As String)
            _virtual_code = Trim(value)
            If _virtual_code.EndsWith(")") Then
                _virtual_code = _virtual_code.Substring(0, _virtual_code.Length - 1)
                _virtual_code = _virtual_code.Replace("(", "-")
            End If
        End Set
    End Property

    Public ReadOnly Property code_htl() As String
        Get
            Return _code & "_" & htl_id
        End Get
    End Property

    Private _notif_type As String
    Public Property notif_type() As String
        Get
            Return _notif_type
        End Get
        Set(ByVal value As String)
            _notif_type = Trim(value)
        End Set
    End Property

    Private _htl_id As String
    Public Property htl_id() As String
        Get
            Return _htl_id
        End Get
        Set(ByVal value As String)
            _htl_id = Trim(value)
        End Set
    End Property

    Private _etl_id As String
    Public Property etl_id() As String
        Get
            Return _etl_id
        End Get
        Set(ByVal value As String)
            _etl_id = Trim(value)
        End Set
    End Property

    Private _operation As String
    Public Property operation() As String
        Get
            Return _operation
        End Get
        Set(ByVal value As String)
            _operation = Trim(value)
        End Set
    End Property

    Private _description As String
    Public Property description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = Trim(value)
        End Set
    End Property

    Private _work_center_txt As String
    Public Property work_center_txt() As String
        Get
            Return _work_center_txt
        End Get
        Set(ByVal value As String)
            _work_center_txt = Trim(value)
        End Set
    End Property

    Private _work_center As String
    Public Property work_center() As String
        Get
            Return _work_center
        End Get
        Set(ByVal value As String)
            _work_center = Trim(value)
        End Set
    End Property

    Private _hours As Double
    Public Property hours() As Double
        Get
            Return _hours
        End Get
        Set(ByVal value As Double)
            _hours = value
        End Set
    End Property

    Private _part_number As String
    Public Property part_number() As String
        Get
            Return _part_number
        End Get
        Set(ByVal value As String)
            _part_number = Trim(value)
        End Set
    End Property

    Private _qty As Double
    Public Property qty() As Double
        Get
            Return _qty
        End Get
        Set(ByVal value As Double)
            _qty = value
        End Set
    End Property


    Private _ata_chapter As String
    Public Property ata_chapter() As String
        Get
            Return _ata_chapter
        End Get
        Set(ByVal value As String)
            _ata_chapter = Trim(value)
        End Set
    End Property

    Private _zone As String
    Public Property zone() As String
        Get
            Return _zone
        End Get
        Set(ByVal value As String)
            _zone = Trim(value)
        End Set
    End Property



    Private _panel As String
    Public Property panel() As String
        Get
            Return _panel
        End Get
        Set(ByVal value As String)
            _panel = Trim(value)
        End Set
    End Property

    Private _pm_ps As String
    Public Property pm_ps() As String
        Get
            Return _pm_ps
        End Get
        Set(ByVal value As String)
            _pm_ps = value
        End Set
    End Property

    Private _src_rev As String
    Public Property src_rev() As String
        Get
            Return _src_rev
        End Get
        Set(ByVal value As String)
            _src_rev = value
        End Set
    End Property

    Private _src_ref As String
    Public Property src_ref() As String
        Get
            Return _src_ref
        End Get
        Set(ByVal value As String)
            _src_ref = value
        End Set
    End Property

    Private _src_type As String
    Public Property src_type() As String
        Get
            Return _src_type
        End Get
        Set(ByVal value As String)
            _src_type = value
        End Set
    End Property
End Class
