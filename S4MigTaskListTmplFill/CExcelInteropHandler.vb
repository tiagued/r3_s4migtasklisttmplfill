﻿Imports System.IO
Imports System.Reflection
Public Class CExcelInteropHandler


    Public Sub New()
        'always need user app for reports
        getUserApp()
    End Sub


    Private _xlappUser As Microsoft.Office.Interop.Excel.Application
    Public ReadOnly Property xlappUser() As Microsoft.Office.Interop.Excel.Application
        Get
            Dim isError As Boolean = True
            Try
                Dim count As Integer = _xlappUser.Workbooks.Count
                isError = False
            Catch ex As Exception
            End Try
            If (isError) Then
                getUserApp()
                _xlappUser.Visible = True
                Return _xlappUser
            Else
                _xlappUser.Visible = True
                Return _xlappUser
            End If
        End Get
    End Property

    'get App of the current user session
    Public Sub getUserApp()
        Dim exists As Boolean = False
        Try
            If Not IsNothing(_xlappUser) Then
                _xlappUser.Visible = True
                exists = True
            End If
        Catch ex As Exception
            exists = False
        End Try
        If Not exists Then
            Try
                _xlappUser = Nothing
                _xlappUser = CType(System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application"), Microsoft.Office.Interop.Excel.Application)
                _xlappUser.Visible = True
            Catch ex As Exception
            End Try
            If IsNothing(_xlappUser) Then
                _xlappUser = New Microsoft.Office.Interop.Excel.Application()
                _xlappUser.Visible = True
            End If
        End If
    End Sub

    Public Sub OnCalc(app As Microsoft.Office.Interop.Excel.Application)
        'not possible when wb is hidden
        Try
            app.Calculation = Microsoft.Office.Interop.Excel.XlCalculation.xlCalculationAutomatic
        Catch ex As System.Runtime.InteropServices.COMException
        End Try
        Try
            app.ScreenUpdating = True
            app.EnableEvents = True
            app.DisplayAlerts = True
        Catch ex As System.Runtime.InteropServices.COMException
            Throw New Exception("An error occured while creating the connexion to the Excel file, Please check that Excel is not in Edit Mode")
        End Try
    End Sub

    Public Sub OffCalc(app As Microsoft.Office.Interop.Excel.Application)
        'not possible when wb is hidden
        Try
            app.Calculation = Microsoft.Office.Interop.Excel.XlCalculation.xlCalculationManual
        Catch ex As System.Runtime.InteropServices.COMException
        End Try
        Try
            app.ScreenUpdating = False
            app.EnableEvents = False
            app.DisplayAlerts = False
        Catch ex As System.Runtime.InteropServices.COMException
            Throw New Exception("An error occured while creating the connexion to the Excel file, Please check that Excel is not in Edit Mode")
        End Try
    End Sub


    'open user App template
    Public Function openxlFile(filepath As String) As Microsoft.Office.Interop.Excel.Workbook
        Dim res As Microsoft.Office.Interop.Excel.Workbook
        Try
            getUserApp()
            OffCalc(_xlappUser)
            res = _xlappUser.Workbooks.Open(filepath, Microsoft.Office.Interop.Excel.XlUpdateLinks.xlUpdateLinksNever, False,,,,,,, True, True,,,,)
            If IsNothing(res) Then
                Dim fileName As String = Path.GetFileName(filepath)
                res = _xlappUser.Workbooks(fileName)
            End If
        Catch ex As Exception
            Throw New Exception("An error occured while instanciating file " & filepath & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        Finally
            OnCalc(_xlappUser)
        End Try
        Return res
    End Function

    'open user App template
    Public Sub safeWorkbookSave(wb As Microsoft.Office.Interop.Excel.Workbook)
        Try
            OffCalc(wb.Application)
            wb.Save()
        Catch ex As Exception
            Throw New Exception("An error occured while saving workbook " & wb.FullName & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        Finally
            OnCalc(wb.Application)
        End Try
    End Sub

    'insert lines for report
    Public Sub insertLines(report_wb As Microsoft.Office.Interop.Excel.Workbook, report_sht As Microsoft.Office.Interop.Excel.Worksheet,
                           xlrg As Microsoft.Office.Interop.Excel.Range, rows As Integer)
        With report_sht.Range(xlrg.Row & ":" & xlrg.Row + rows - 1)
            .Insert()
        End With
    End Sub


    Private Function IsFileLocked(exception As Exception) As Boolean
        Try
            Dim errorCode As Integer = Runtime.InteropServices.Marshal.GetHRForException(exception) And ((1 << 16) - 1)
            Return errorCode = 32 OrElse errorCode = 33 OrElse errorCode = 1726 OrElse errorCode = 1722
        Catch ex As Exception
            Return False
        End Try
    End Function


    'Release the objects
    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

End Class
