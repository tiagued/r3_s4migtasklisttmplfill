﻿Public Class CConfigController
    Public controller As CMasterController
    Public Sub New(ctrl As CMasterController)
        controller = ctrl
    End Sub
    Public Const CONF_FILE_MAIN_SHEET As String = "conf_tbl"
    Public Const CONF_MASTER_TBL As String = "master_config_tbl"
    'list of items to load
    Public masterList As List(Of CMasterConfObj)

    'ac applicability table
    Public Const AC_APPL_LIST_NAME As String = "ac_appl_tbl"
    Public _ac_appl_list As Dictionary(Of String, List(Of CAircraftApplicability))
    'source col config
    Public Const SRC_CONF_LIST As String = "bow_config"
    Public srcDataConfList As Dictionary(Of String, String)
    'template table conf
    Public Const TMPL_COL_CONF_LIST As String = "tmpl_col_config"
    Public tmplDataConfList As Dictionary(Of String, String)
    Public tmplDataConfListReverse As Dictionary(Of Integer, String)

    'default value table
    Public defaultValueList As Dictionary(Of String, String)
    Public Const DEFAULT_VAL_CONF_LIST As String = "default_values_tbl"
    'Mtype correspondance Table
    Public mtypeCorrespondanceList As Dictionary(Of String, String)
    Public Const MTYPE_CORRESPD_LIST As String = "m_type_correspondance_tbl"
    'work center correspondance table
    Public workCenterCorrespondanceList As Dictionary(Of String, String)
    Public Const WORK_CENTER_CORRESPD_LIST As String = "workcenter_corresp_tbl"
    'pm ps correspondance table
    Public PMPSCorrespondanceList As Dictionary(Of String, String)
    Public Const PMPS_CORRESPD_LIST As String = "pmps_corresp_tbl"


    Public wb_conf As Microsoft.Office.Interop.Excel.Workbook


    'opend and load confing
    Public Sub loadConfing(filePath)
        wb_conf = Nothing
        Dim wb_conf_main_sht As Microsoft.Office.Interop.Excel.Worksheet = Nothing
        Dim listO As Microsoft.Office.Interop.Excel.ListObject = Nothing
        Dim idList As New List(Of String)
        Try
            If Trim(filePath) = "" Then
                Throw New Exception("The config file path is empty")
            End If
            If Not IO.File.Exists(filePath) Then
                Throw New Exception("The file " & filePath & "does not exists")
            End If
            wb_conf = controller.xlFileHandler.openxlFile(filePath)
            wb_conf_main_sht = wb_conf.Worksheets(CONF_FILE_MAIN_SHEET)


            'read ac appl list
            _ac_appl_list = New Dictionary(Of String, List(Of CAircraftApplicability))
            listO = wb_conf_main_sht.ListObjects(AC_APPL_LIST_NAME)
            For line As Integer = 1 To listO.ListRows.Count Step 1
                Dim item As New CAircraftApplicability
                item.id = listO.DataBodyRange(line, listO.ListColumns("id").Index).value
                item.ac_manuf = listO.DataBodyRange(line, listO.ListColumns("ac_manufacturer").Index).value
                item.ac_type = listO.DataBodyRange(line, listO.ListColumns("ac_type").Index).value
                item.ac_model = listO.DataBodyRange(line, listO.ListColumns("ac_model").Index).value

                If String.IsNullOrWhiteSpace(item.id) OrElse String.IsNullOrWhiteSpace(item.ac_manuf) OrElse String.IsNullOrWhiteSpace(item.ac_type) OrElse String.IsNullOrWhiteSpace(item.ac_model) Then
                    Dim Log As New CLogEntry
                    Log.line = 0
                    Log.mat_id = 0
                    Log.code = "config"
                    Log.column = "A/C Applicability"
                    Log.log_type = CLogEntry.LOG_TYPE_ERROR
                    Log.description = "The id, the A/C Manufacturer, the A/C type and the A/C Model must not be empty"
                    controller.logGlobalList.Add(Log)
                Else
                    If Not _ac_appl_list.ContainsKey(item.id) Then
                        _ac_appl_list.Add(item.id, New List(Of CAircraftApplicability))
                    End If
                    _ac_appl_list(item.id).Add(item)
                End If
            Next

            'read main config list
            masterList = New List(Of CMasterConfObj)
            listO = wb_conf_main_sht.ListObjects(CONF_MASTER_TBL)
            For line As Integer = 1 To listO.ListRows.Count Step 1
                Dim confObj As New CMasterConfObj
                For col As Integer = 1 To listO.ListColumns.Count Step 1
                    Dim colName As String = listO.ListColumns(col).Name
                    Dim val As Object = listO.DataBodyRange(line, listO.ListColumns(colName).Index).value
                    CallByName(confObj, colName, CallType.Let, New Object() {val})
                Next
                If confObj.upload Then
                    confObj.appl_list = _ac_appl_list(confObj.id)
                    masterList.Add(confObj)
                End If

                If Not idList.Contains(confObj.id) Then
                    idList.Add(confObj.id)
                Else
                    Throw New Exception("The id " & confObj.id & " appears twice in the table " & listO.Name)
                End If
                'check values
                'check src_id
                If String.IsNullOrWhiteSpace(confObj.src_id_ac_type_text) Then
                    Dim Log As New CLogEntry
                    Log.line = 0
                    Log.mat_id = 0
                    Log.code = "config"
                    Log.column = "src_id_ac_type_text"
                    Log.log_type = CLogEntry.LOG_TYPE_ERROR
                    Log.description = "The source id prefix must not be empty in the config entry id = " & confObj.id
                    controller.logGlobalList.Add(Log)
                End If
                If String.IsNullOrWhiteSpace(confObj.engineering_dept) Then
                    Dim Log As New CLogEntry
                    Log.line = 0
                    Log.mat_id = 0
                    Log.code = "config"
                    Log.column = "engineering_dept"
                    Log.log_type = CLogEntry.LOG_TYPE_ERROR
                    Log.description = "The engineering department must not be empty in the config entry id = " & confObj.id
                    controller.logGlobalList.Add(Log)
                End If
                If String.IsNullOrWhiteSpace(confObj.issue_dt) Then
                    Dim Log As New CLogEntry
                    Log.line = 0
                    Log.mat_id = 0
                    Log.code = "config"
                    Log.column = "issue_dt"
                    Log.log_type = CLogEntry.LOG_TYPE_ERROR
                    Log.description = "The issue date must not be empty in the config entry id = " & confObj.id
                    controller.logGlobalList.Add(Log)
                End If
            Next


            'read source data conf
            srcDataConfList = New Dictionary(Of String, String)
            listO = wb_conf_main_sht.ListObjects(SRC_CONF_LIST)
            For line As Integer = 1 To listO.ListRows.Count Step 1
                If Not srcDataConfList.ContainsKey(listO.DataBodyRange(line, 1).value) Then
                    srcDataConfList.Add(listO.DataBodyRange(line, 1).value, listO.DataBodyRange(line, 2).value)
                Else
                    Throw New Exception("The key " & listO.DataBodyRange(line, 1) & " appears twice in the table " & listO.Name)
                End If
            Next
            'read template data conf
            tmplDataConfList = New Dictionary(Of String, String)
            tmplDataConfListReverse = New Dictionary(Of Integer, String)
            listO = wb_conf_main_sht.ListObjects(TMPL_COL_CONF_LIST)
            For line As Integer = 1 To listO.ListRows.Count Step 1
                If Not tmplDataConfList.ContainsKey(listO.DataBodyRange(line, 1).value) Then
                    tmplDataConfList.Add(listO.DataBodyRange(line, 1).value, listO.DataBodyRange(line, 2).value)
                Else
                    Throw New Exception("The key " & listO.DataBodyRange(line, 1) & " appears twice in the table " & listO.Name)
                End If
                If Not tmplDataConfListReverse.ContainsKey(listO.DataBodyRange(line, 2).value) Then
                    tmplDataConfListReverse.Add(listO.DataBodyRange(line, 2).value, listO.DataBodyRange(line, 1).value)
                Else
                    Throw New Exception("The key " & listO.DataBodyRange(line, 2) & " appears twice in the table " & listO.Name)
                End If
            Next

            'read default values
            defaultValueList = New Dictionary(Of String, String)
            listO = wb_conf_main_sht.ListObjects(DEFAULT_VAL_CONF_LIST)
            For line As Integer = 1 To listO.ListRows.Count Step 1
                If Not defaultValueList.ContainsKey(listO.DataBodyRange(line, 1).value) Then
                    defaultValueList.Add(listO.DataBodyRange(line, 1).value, listO.DataBodyRange(line, 2).value)
                Else
                    Throw New Exception("The key " & listO.DataBodyRange(line, 1) & " appears twice in the table " & listO.Name)
                End If
            Next
            'check default values rules
            'mtype correspondance
            mtypeCorrespondanceList = New Dictionary(Of String, String)
            listO = wb_conf_main_sht.ListObjects(MTYPE_CORRESPD_LIST)
            For line As Integer = 1 To listO.ListRows.Count Step 1
                If Not mtypeCorrespondanceList.ContainsKey(UCase(listO.DataBodyRange(line, 1).value)) Then
                    mtypeCorrespondanceList.Add(UCase(listO.DataBodyRange(line, 1).value), listO.DataBodyRange(line, 2).value)
                Else
                    Throw New Exception("The key " & CStr(listO.DataBodyRange(line, 1)) & " appears twice in the table " & listO.Name)
                End If
            Next
            'work center correspondance
            workCenterCorrespondanceList = New Dictionary(Of String, String)
            listO = wb_conf_main_sht.ListObjects(WORK_CENTER_CORRESPD_LIST)
            For line As Integer = 1 To listO.ListRows.Count Step 1
                If Not String.IsNullOrWhiteSpace(listO.DataBodyRange(line, 1).value) Then
                    If Not workCenterCorrespondanceList.ContainsKey(UCase(listO.DataBodyRange(line, 1).value)) Then
                        workCenterCorrespondanceList.Add(UCase(listO.DataBodyRange(line, 1).value), listO.DataBodyRange(line, 2).value)
                    Else
                        Throw New Exception("The key " & listO.DataBodyRange(line, 1).value & " appears twice in the table " & listO.Name)
                    End If
                End If
            Next

            'PMPS correspondance
            PMPSCorrespondanceList = New Dictionary(Of String, String)
            listO = wb_conf_main_sht.ListObjects(PMPS_CORRESPD_LIST)
            For line As Integer = 1 To listO.ListRows.Count Step 1
                If Not String.IsNullOrWhiteSpace(listO.DataBodyRange(line, 1).value) Then
                    If Not PMPSCorrespondanceList.ContainsKey(UCase(listO.DataBodyRange(line, 1).value)) Then
                        PMPSCorrespondanceList.Add(UCase(listO.DataBodyRange(line, 1).value), listO.DataBodyRange(line, 2).value)
                    Else
                        Throw New Exception("The key " & listO.DataBodyRange(line, 1).value & " appears twice in the table " & listO.Name)
                    End If
                End If
            Next

        Catch ex As Exception
            Throw New Exception("An error occured while loading the config " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub
End Class
