﻿Public Class CTmplEntry
    Public Sub New()
        _is_fully_loaded = False
    End Sub

    Public Const M_TYPE_HTL As String = "HTL"
    Public Const M_TYPE_ETL As String = "ETL"

    Public htl_entry As CTmplEntry
    Public etl_entry As CTmplEntry
    'in case of panel
    Public etl_entry_close_panel As CTmplEntry
    'list of operation for ETL
    Public etlOperationList As List(Of CTmplEntry)
    'list of material for ETL
    Public etlMaterialList As List(Of CTmplEntry)
    'list of applicability for ETL
    Public etlApplList As List(Of CTmplEntry)
    'list of material and operation
    Public etlMaterialOperationList As List(Of CTmplEntry)
    'list of related HTL for HTL
    Public htlRelatedHtlList As Dictionary(Of String, CTmplEntry)
    'list of HTL parent for HTL
    Public htlParentHtlList As List(Of CTmplEntry)
    'list of related ETLs for HTL
    Public htlRelatedEtlList As List(Of CTmplEntry)
    'list of links to report
    Public htlAllRelatedObjList As List(Of CTmplEntry)
    'list of other A/C applicabilities
    Public mbx_ac_applicability_list As List(Of CTmplEntry)

    Public gqt_entry As BOWEntry

    Private _is_panel As Boolean
    Public Property is_panel() As Boolean
        Get
            Return _is_panel
        End Get
        Set(ByVal value As Boolean)
            _is_panel = value
        End Set
    End Property

    Private _line As Integer
    Public Property line() As Integer
        Get
            Return _line
        End Get
        Set(ByVal value As Integer)
            _line = value
        End Set
    End Property
    'use to track if the object has been fully loaded from the source
    Private _is_fully_loaded As Boolean
    Public Property is_fully_loaded() As Boolean
        Get
            Return _is_fully_loaded
        End Get
        Set(ByVal value As Boolean)
            _is_fully_loaded = value
        End Set
    End Property

    Private _source_id As String
    Public Property source_id() As String
        Get
            Return _source_id
        End Get
        Set(ByVal value As String)
            _source_id = value
        End Set
    End Property

    Private _m_type As String
    Public Property m_type() As String
        Get
            Return _m_type
        End Get
        Set(ByVal value As String)
            _m_type = value
        End Set
    End Property

    Private _seq_id As Integer
    Public Property seq_id() As Integer
        Get
            Return _seq_id
        End Get
        Set(ByVal value As Integer)
            _seq_id = value
        End Set
    End Property

    Private _child As String
    Public Property child() As String
        Get
            Return _child
        End Get
        Set(ByVal value As String)
            _child = value
        End Set
    End Property

    Private _description As String
    Public Property description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    Private _long_text As String
    Public Property long_text() As String
        Get
            Return _long_text
        End Get
        Set(ByVal value As String)
            _long_text = value
        End Set
    End Property

    Private _external_ref_no As String
    Public Property external_ref_no() As String
        Get
            Return _external_ref_no
        End Get
        Set(ByVal value As String)
            _external_ref_no = value
        End Set
    End Property

    Private _mod_auth As String
    Public Property mod_auth() As String
        Get
            Return _mod_auth
        End Get
        Set(ByVal value As String)
            _mod_auth = value
        End Set
    End Property


    Private _repeat_application As String
    Public Property repeat_application() As String
        Get
            Return _repeat_application
        End Get
        Set(ByVal value As String)
            _repeat_application = value
        End Set
    End Property

    Private _engineering_dept As String
    Public Property engineering_dept() As String
        Get
            Return _engineering_dept
        End Get
        Set(ByVal value As String)
            _engineering_dept = value
        End Set
    End Property

    Private _pplant As String
    Public Property pplant() As String
        Get
            Return _pplant
        End Get
        Set(ByVal value As String)
            _pplant = value
        End Set
    End Property

    Private _issue_dt As String
    Public Property issue_dt() As String
        Get
            Return _issue_dt
        End Get
        Set(ByVal value As String)
            _issue_dt = value
        End Set
    End Property

    Private _ata As String
    Public Property ata() As String
        Get
            Return _ata
        End Get
        Set(ByVal value As String)
            _ata = value
        End Set
    End Property

    Private _ac_type As String
    Public Property ac_type() As String
        Get
            Return _ac_type
        End Get
        Set(ByVal value As String)
            _ac_type = value
        End Set
    End Property

    Private _ac_manufacturer As String
    Public Property ac_manufacturer() As String
        Get
            Return _ac_manufacturer
        End Get
        Set(ByVal value As String)
            _ac_manufacturer = value
        End Set
    End Property

    Private _ac_model As String
    Public Property ac_model() As String
        Get
            Return _ac_model
        End Get
        Set(ByVal value As String)
            _ac_model = value
        End Set
    End Property

    Private _ac_serial_number As String
    Public Property ac_serial_number() As String
        Get
            Return _ac_serial_number
        End Get
        Set(ByVal value As String)
            _ac_serial_number = value
        End Set
    End Property

    Private _oem_ja_code As String
    Public Property oem_ja_code() As String
        Get
            Return _oem_ja_code
        End Get
        Set(ByVal value As String)
            _oem_ja_code = value
        End Set
    End Property

    Private _oem_doc_ref As String
    Public Property oem_doc_ref() As String
        Get
            Return _oem_doc_ref
        End Get
        Set(ByVal value As String)
            _oem_doc_ref = value
        End Set
    End Property

    Private _oem_doc_rev As String
    Public Property oem_doc_rev() As String
        Get
            Return _oem_doc_rev
        End Get
        Set(ByVal value As String)
            _oem_doc_rev = value
        End Set
    End Property

    Private _oem_doc_type As String
    Public Property oem_doc_type() As String
        Get
            Return _oem_doc_type
        End Get
        Set(ByVal value As String)
            _oem_doc_type = value
        End Set
    End Property

    Private _serv_material As String
    Public Property serv_material() As String
        Get
            Return _serv_material
        End Get
        Set(ByVal value As String)
            _serv_material = value
        End Set
    End Property

    Private _parent_tl_ref_id As String
    Public Property parent_tl_ref_id() As String
        Get
            Return _parent_tl_ref_id
        End Get
        Set(ByVal value As String)
            _parent_tl_ref_id = value
        End Set
    End Property

    Private _child_tl_ref_id As String
    Public Property child_tl_ref_id() As String
        Get
            Return _child_tl_ref_id
        End Get
        Set(ByVal value As String)
            _child_tl_ref_id = value
        End Set
    End Property

    Private _dir_source_id As String
    Public Property dir_source_id() As String
        Get
            Return _dir_source_id
        End Get
        Set(ByVal value As String)
            _dir_source_id = value
        End Set
    End Property

    Private _tl_type As String
    Public Property tl_type() As String
        Get
            Return _tl_type
        End Get
        Set(ByVal value As String)
            _tl_type = value
        End Set
    End Property


    Private _tl_description As String
    Public Property tl_description() As String
        Get
            Return _tl_description
        End Get
        Set(ByVal value As String)
            _tl_description = value
        End Set
    End Property

    Private _plant As String
    Public Property plant() As String
        Get
            Return _plant
        End Get
        Set(ByVal value As String)
            _plant = value
        End Set
    End Property

    Private _tl_ext_id As String
    Public Property tl_ext_id() As String
        Get
            Return _tl_ext_id
        End Get
        Set(ByVal value As String)
            _tl_ext_id = value
        End Set
    End Property

    Private _work_center As String
    Public Property work_center() As String
        Get
            Return _work_center
        End Get
        Set(ByVal value As String)
            _work_center = value
        End Set
    End Property

    Private _status As String
    Public Property status() As String
        Get
            Return _status
        End Get
        Set(ByVal value As String)
            _status = value
        End Set
    End Property

    Private _planner_group As String
    Public Property planner_group() As String
        Get
            Return _planner_group
        End Get
        Set(ByVal value As String)
            _planner_group = value
        End Set
    End Property

    Private _usage As String
    Public Property usage() As String
        Get
            Return _usage
        End Get
        Set(ByVal value As String)
            _usage = value
        End Set
    End Property

    Private _profile As String
    Public Property profile() As String
        Get
            Return _profile
        End Get
        Set(ByVal value As String)
            _profile = value
        End Set
    End Property

    Private _pm_ps As String
    Public Property pm_ps() As String
        Get
            Return _pm_ps
        End Get
        Set(ByVal value As String)
            _pm_ps = value
        End Set
    End Property

    Private _operation_no As String
    Public Property operation_no() As String
        Get
            Return _operation_no
        End Get
        Set(ByVal value As String)
            _operation_no = value
        End Set
    End Property

    Private _op_plant As String
    Public Property op_plant() As String
        Get
            Return _op_plant
        End Get
        Set(ByVal value As String)
            _op_plant = value
        End Set
    End Property

    Private _op_work_center As String
    Public Property op_work_center() As String
        Get
            Return _op_work_center
        End Get
        Set(ByVal value As String)
            _op_work_center = value
        End Set
    End Property

    Private _op_control_key As String
    Public Property op_control_key() As String
        Get
            Return _op_control_key
        End Get
        Set(ByVal value As String)
            _op_control_key = value
        End Set
    End Property

    Private _op_work_value As Double
    Public Property op_work_value() As Double
        Get
            Return _op_work_value
        End Get
        Set(ByVal value As Double)
            _op_work_value = value
        End Set
    End Property

    Private _op_work_unit As String
    Public Property op_work_unit() As String
        Get
            Return _op_work_unit
        End Get
        Set(ByVal value As String)
            _op_work_unit = value
        End Set
    End Property

    Private _op_stand_id As String
    Public Property op_stand_id() As String
        Get
            Return _op_stand_id
        End Get
        Set(ByVal value As String)
            _op_stand_id = value
        End Set
    End Property

    Private _op_calculate_effort As String
    Public Property op_calculate_effort() As String
        Get
            Return _op_calculate_effort
        End Get
        Set(ByVal value As String)
            _op_calculate_effort = value
        End Set
    End Property

    Private _op_text As String
    Public Property op_text() As String
        Get
            Return _op_text
        End Get
        Set(ByVal value As String)
            _op_text = value
        End Set
    End Property

    Private _op_long_text As String
    Public Property op_long_text() As String
        Get
            Return _op_long_text
        End Get
        Set(ByVal value As String)
            _op_long_text = value
        End Set
    End Property

    Private _mat_operation_no As String
    Public Property mat_operation_no() As String
        Get
            Return _mat_operation_no
        End Get
        Set(ByVal value As String)
            _mat_operation_no = value
        End Set
    End Property

    Private _mat_comp_mat As String
    Public Property mat_comp_mat() As String
        Get
            Return _mat_comp_mat
        End Get
        Set(ByVal value As String)
            _mat_comp_mat = value
        End Set
    End Property

    Private _mat_comp_qty As String
    Public Property mat_comp_qty() As String
        Get
            Return _mat_comp_qty
        End Get
        Set(ByVal value As String)
            _mat_comp_qty = value
        End Set
    End Property

    Private _tlc_ac_manuf As String
    Public Property tlc_ac_manuf() As String
        Get
            Return _tlc_ac_manuf
        End Get
        Set(ByVal value As String)
            _tlc_ac_manuf = value
        End Set
    End Property

    Private _key_date As String
    Public Property key_date() As String
        Get
            Return _key_date
        End Get
        Set(ByVal value As String)
            _key_date = value
        End Set
    End Property

    'ETL characteristics
    Private _tlc_ac_type As String
    Public Property tlc_ac_type() As String
        Get
            Return _tlc_ac_type
        End Get
        Set(ByVal value As String)
            _tlc_ac_type = value
        End Set
    End Property

    Private _tlc_ac_model As String
    Public Property tlc_ac_model() As String
        Get
            Return _tlc_ac_model
        End Get
        Set(ByVal value As String)
            _tlc_ac_model = value
        End Set
    End Property

    Private _tlc_tl_task_type As String
    Public Property tlc_tl_task_type() As String
        Get
            Return _tlc_tl_task_type
        End Get
        Set(ByVal value As String)
            _tlc_tl_task_type = value
        End Set
    End Property

    Private _tlc_tl_panel_code As String
    Public Property tlc_tl_panel_code() As String
        Get
            Return _tlc_tl_panel_code
        End Get
        Set(ByVal value As String)
            _tlc_tl_panel_code = value
        End Set
    End Property

    Private _tlc_tl_ac_zone As String
    Public Property tlc_tl_ac_zone() As String
        Get
            Return _tlc_tl_ac_zone
        End Get
        Set(ByVal value As String)
            _tlc_tl_ac_zone = value
        End Set
    End Property

    Private _tlc_tl_critical_task As String
    Public Property tlc_tl_critical_task() As String
        Get
            Return _tlc_tl_critical_task
        End Get
        Set(ByVal value As String)
            _tlc_tl_critical_task = value
        End Set
    End Property


End Class
