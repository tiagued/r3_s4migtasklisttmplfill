﻿SELECT DISTINCT UPPER(LTRIM(RTRIM(COMP2.Component_Ref))) as MATERIAL,
   REPLACE (STUFF((
        SELECT DISTINCT ' // ' + LTRIM(RTRIM(ISNULL(AC_MAN.ACM,''))) + ' ' +  LTRIM(RTRIM(ISNULL(AC_TYPE.ACT_SAP,''))) + ' (' +  LTRIM(RTRIM(ISNULL(TASK.Code,'')))+ ') '+ LTRIM(RTRIM(ISNULL(TASK.Description,'')))
        FROM dbo.GQ_AC_Types_tbl as AC_TYPE INNER JOIN
			dbo.GQ_AC_Manufacturers_tbl as AC_MAN on AC_MAN.ACMid = AC_TYPE.ACMid INNER JOIN 
			dbo.GQ_Components_tbl as COMP ON COMP.ACTid = AC_TYPE.ACTid INNER JOIN
			dbo.GQ_Materials_tbl as TASK ON TASK.MATid = COMP.ParentMATid
        WHERE COMP.Component_Ref  = COMP2.Component_Ref
        FOR XML PATH('')), 1, 4, ''
    ),'// ',CHAR(10)) AS APPLICABILITY
FROM dbo.GQ_Components_tbl as COMP2 
INNER JOIN
dbo.GQ_Calculation_tbl ON COMP2.COMPid = dbo.GQ_Calculation_tbl.COMPid
WHERE UPPER (COMP2.Component_Ref)  not like '%LABOR%' AND
	  UPPER (COMP2.Component_Ref)  not like '%FLAT%' AND
	  UPPER (COMP2.Component_Ref)  not like '%SERVICE%' AND
	  UPPER (COMP2.Component_Ref)  not like '%SMALL MAT%' AND
	  UPPER (COMP2.Component_Ref)  not like '%TBD%' AND
	  UPPER (COMP2.Component_Ref)  not like '%ACCESS PANEL GROUP%' AND
	  UPPER (COMP2.Component_Desc)  not like '%ACCESS PANEL GROUP%' AND	  
	  LEN (COMP2.Component_Ref)>1   
	  
GROUP BY COMP2.Component_Ref;

SELECT DISTINCT UPPER(LTRIM(RTRIM(COMP2.Component_Ref))) as MATERIAL,
   REPLACE (STUFF((
        SELECT DISTINCT ' // ' + LTRIM(RTRIM(ISNULL(AC_MAN.ACM,''))) + ' ' +  LTRIM(RTRIM(ISNULL(AC_TYPE.ACT_SAP,''))) + ' (' +  LTRIM(RTRIM(ISNULL(TASK.Code,'')))+ ') '+ LTRIM(RTRIM(ISNULL(TASK.Description,'')))
        FROM dbo.GQ_AC_Types_tbl as AC_TYPE INNER JOIN
			dbo.GQ_AC_Manufacturers_tbl as AC_MAN on AC_MAN.ACMid = AC_TYPE.ACMid INNER JOIN 
			dbo.GQ_Components_tbl as COMP ON COMP.ACTid = AC_TYPE.ACTid INNER JOIN
			dbo.GQ_Materials_tbl as TASK ON TASK.MATid = COMP.ParentMATid
        WHERE COMP.Component_Ref  = COMP2.Component_Ref
        FOR XML PATH('')), 1, 4, ''
    ),'// ',CHAR(10)) AS APPLICABILITY
FROM dbo.GQ_Components_tbl as COMP2 
WHERE UPPER (COMP2.Component_Ref)  not like '%LABOR%' AND
	  UPPER (COMP2.Component_Ref)  not like '%FLAT%' AND
	  UPPER (COMP2.Component_Ref)  not like '%SERVICE%' AND
	  UPPER (COMP2.Component_Ref)  not like '%SMALL MAT%' AND
	  UPPER (COMP2.Component_Ref)  not like '%TBD%' AND
	  UPPER (COMP2.Component_Ref)  not like '%ACCESS PANEL GROUP%' AND
	  UPPER (COMP2.Component_Desc)  not like '%ACCESS PANEL GROUP%' AND	  
	  LEN (COMP2.Component_Ref)>1 AND
	  COMP2.MATid is null
	  
GROUP BY COMP2.Component_Ref