﻿SELECT DISTINCT LTRIM(RTRIM(dbo.GQ_AC_Manufacturers_tbl.ACM)) as OEM, LTRIM(RTRIM(dbo.GQ_AC_Types_tbl.ACT_SAP)) as AC_TYPE, UPPER(LTRIM(RTRIM(dbo.GQ_Components_tbl.Component_Ref))) as MATERIAL, LTRIM(RTRIM(dbo.GQ_Components_tbl.Component_Desc)) as MAT_DESC, LEN(LTRIM(RTRIM(dbo.GQ_Components_tbl.Component_Desc))) as MAT_DESC_LEN
FROM dbo.GQ_AC_Types_tbl INNER JOIN
dbo.GQ_AC_Manufacturers_tbl on dbo.GQ_AC_Manufacturers_tbl.ACMid = dbo.GQ_AC_Types_tbl.ACMid
INNER JOIN
dbo.GQ_Components_tbl ON dbo.GQ_Components_tbl.ACTid = dbo.GQ_AC_Types_tbl.ACTid
INNER JOIN
dbo.GQ_Calculation_tbl ON dbo.GQ_Components_tbl.COMPid = dbo.GQ_Calculation_tbl.COMPid

WHERE UPPER (dbo.GQ_Components_tbl.Component_Ref)  not like '%LABOR%' AND
	  UPPER (dbo.GQ_Components_tbl.Component_Ref)  not like '%FLAT%' AND
	  UPPER (dbo.GQ_Components_tbl.Component_Ref)  not like '%SERVICE%' AND
	  UPPER (dbo.GQ_Components_tbl.Component_Ref)  not like '%SMALL MAT%' AND
	  UPPER (dbo.GQ_Components_tbl.Component_Ref)  not like '%TBD%' AND
	  UPPER (dbo.GQ_Components_tbl.Component_Ref)  not like '%ACCESS PANEL GROUP%' AND
	  UPPER (dbo.GQ_Components_tbl.Component_Desc)  not like '%ACCESS PANEL GROUP%' AND	  
	  LEN (dbo.GQ_Components_tbl.Component_Ref)>1 AND
	  UPPER(dbo.GQ_AC_Manufacturers_tbl.ACM) not in ('COMMON', 'RMU','TEST')
ORDER BY OEM, AC_TYPE, MATERIAL, MAT_DESC_LEN DESC;

--dbo.GQ_Calculation_tbl.Description = 'MATERIAL'
